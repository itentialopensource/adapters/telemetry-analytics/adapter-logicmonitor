/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-logicmonitor',
      type: 'Logicmonitor',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Logicmonitor = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Logicmonitor Adapter Test', () => {
  describe('Logicmonitor Class Tests', () => {
    const a = new Logicmonitor(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let devicesId = 'fakedata';
    let devicesName = 'fakedata';
    let devicesDeviceId = 555;
    const devicesAddDeviceBodyParam = {
      displayName: 'Cisco Router',
      preferredCollectorId: 2,
      name: 'Main Collector'
    };
    describe('#addDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDevice(null, null, null, devicesAddDeviceBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.netflowCollectorGroupName);
                assert.equal(4, data.response.azureState);
                assert.equal(-1, data.response.relatedDeviceId);
                assert.equal('Cisco Router', data.response.displayName);
                assert.equal('www.ciscorouter.com', data.response.link);
                assert.equal(1, data.response.awsState);
                assert.equal('This is a Cisco Router', data.response.description);
                assert.equal(true, data.response.disableAlerting);
                assert.equal(7, data.response.netflowCollectorGroupId);
                assert.equal(7, data.response.createdOn);
                assert.equal(true, Array.isArray(data.response.systemProperties));
                assert.equal('string', data.response.hostStatus);
                assert.equal(1, data.response.gcpState);
                assert.equal(5, data.response.autoBalancedCollectorGroupId);
                assert.equal(10, data.response.autoPropsUpdatedOn);
                assert.equal(8, data.response.scanConfigId);
                assert.equal(true, Array.isArray(data.response.inheritedProperties));
                assert.equal(1, data.response.id);
                assert.equal(true, data.response.enableNetflow);
                assert.equal(8, data.response.lastDataTime);
                assert.equal('16,4,3', data.response.hostGroupIds);
                assert.equal(2, data.response.upTimeInSeconds);
                assert.equal(3, data.response.deviceType);
                assert.equal(1, data.response.currentCollectorId);
                assert.equal('string', data.response.netflowCollectorDescription);
                assert.equal(1, data.response.netflowCollectorId);
                assert.equal('string', data.response.userPermission);
                assert.equal(9, data.response.autoPropsAssignedOn);
                assert.equal(5, data.response.updatedOn);
                assert.equal('string', data.response.preferredCollectorGroupName);
                assert.equal(9, data.response.preferredCollectorGroupId);
                assert.equal(true, Array.isArray(data.response.autoProperties));
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal(8, data.response.toDeleteTimeInMs);
                assert.equal('string', data.response.collectorDescription);
                assert.equal(2, data.response.preferredCollectorId);
                assert.equal(2, data.response.lastRawdataTime);
                assert.equal('Main Collector', data.response.name);
                assert.equal(2, data.response.deletedTimeInMs);
              } else {
                runCommonAsserts(data, error);
              }
              devicesId = data.response.id;
              devicesName = data.response.name;
              devicesDeviceId = data.response.id;
              saveMockData('Devices', 'addDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesAddDevicePropertyBodyParam = {
      inheritList: [
        {
          fullpath: 'string',
          id: 7,
          type: 'string',
          value: 'string'
        }
      ],
      name: 'string',
      type: 'string',
      value: 'string'
    };
    describe('#addDeviceProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDeviceProperty(devicesDeviceId, devicesAddDevicePropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.inheritList));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'addDeviceProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduleAutoDiscoveryByDeviceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scheduleAutoDiscoveryByDeviceId(null, null, null, devicesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'scheduleAutoDiscoveryByDeviceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceList(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceInstanceAlertSettingListOfDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceAlertSettingListOfDevice(null, null, null, devicesDeviceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceDatasourceInstanceAlertSettingListOfDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceList(devicesDeviceId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceDatasourceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceById(devicesDeviceId, devicesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.alertStatus);
                assert.equal(false, data.response.autoDiscovery);
                assert.equal('string', data.response.dataSourceDisplayName);
                assert.equal(1, data.response.deviceId);
                assert.equal('string', data.response.deviceName);
                assert.equal(1, data.response.createdOn);
                assert.equal('string', data.response.collectMethod);
                assert.equal(8, data.response.dataSourceId);
                assert.equal(true, Array.isArray(data.response.graphs));
                assert.equal('string', data.response.sdtAt);
                assert.equal(2, data.response.nextAutoDiscoveryOn);
                assert.equal(5, data.response.id);
                assert.equal(7, data.response.alertStatusPriority);
                assert.equal('string', data.response.alertDisableStatus);
                assert.equal('string', data.response.dataSourceDescription);
                assert.equal(true, Array.isArray(data.response.overviewGraphs));
                assert.equal(false, data.response.stopMonitoring);
                assert.equal(7, data.response.assignedOn);
                assert.equal(false, data.response.isMultiple);
                assert.equal(10, data.response.instanceNumber);
                assert.equal(1, data.response.updatedOn);
                assert.equal('string', data.response.sdtStatus);
                assert.equal('string', data.response.dataSourceName);
                assert.equal('string', data.response.deviceDisplayName);
                assert.equal(4, data.response.monitoringInstanceNumber);
                assert.equal(true, Array.isArray(data.response.groupsDisabledThisSource));
                assert.equal('string', data.response.groupName);
                assert.equal(false, data.response.instanceAutoGroupEnabled);
                assert.equal('string', data.response.dataSourceType);
                assert.equal(3, data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceDatasourceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceDataById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceDataById(devicesDeviceId, devicesId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.instances);
                assert.equal(true, Array.isArray(data.response.dataPoints));
                assert.equal('string', data.response.nextPageParams);
                assert.equal('string', data.response.dataSourceName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceDatasourceDataById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDTHistoryByDeviceDataSourceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDTHistoryByDeviceDataSourceId(devicesDeviceId, devicesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getSDTHistoryByDeviceDataSourceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicePropertyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevicePropertyList(devicesDeviceId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDevicePropertyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateDevicePropertyByNameBodyParam = {
      inheritList: [
        {
          fullpath: 'string',
          id: 6,
          type: 'string',
          value: 'string'
        }
      ],
      name: 'string',
      type: 'string',
      value: 'string'
    };
    describe('#updateDevicePropertyByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDevicePropertyByName(devicesDeviceId, devicesName, devicesUpdateDevicePropertyByNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateDevicePropertyByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesPatchDevicePropertyByNameBodyParam = {
      inheritList: [
        {
          fullpath: 'string',
          id: 2,
          type: 'string',
          value: 'string'
        }
      ],
      name: 'string',
      type: 'string',
      value: 'string'
    };
    describe('#patchDevicePropertyByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDevicePropertyByName(devicesDeviceId, devicesName, devicesPatchDevicePropertyByNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'patchDevicePropertyByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicePropertyByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevicePropertyByName(devicesDeviceId, devicesName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.inheritList));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDevicePropertyByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateDeviceBodyParam = {
      displayName: 'Cisco Router',
      preferredCollectorId: 2,
      name: 'Main Collector'
    };
    describe('#updateDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDevice(null, null, null, devicesId, devicesUpdateDeviceBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesPatchDeviceBodyParam = {
      displayName: 'Cisco Router',
      preferredCollectorId: 2,
      name: 'Main Collector'
    };
    describe('#patchDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDevice(null, null, null, devicesId, devicesPatchDeviceBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'patchDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceById(null, null, null, devicesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.netflowCollectorGroupName);
                assert.equal(3, data.response.azureState);
                assert.equal(-1, data.response.relatedDeviceId);
                assert.equal('Cisco Router', data.response.displayName);
                assert.equal('www.ciscorouter.com', data.response.link);
                assert.equal(9, data.response.awsState);
                assert.equal('This is a Cisco Router', data.response.description);
                assert.equal(true, data.response.disableAlerting);
                assert.equal(7, data.response.netflowCollectorGroupId);
                assert.equal(4, data.response.createdOn);
                assert.equal(true, Array.isArray(data.response.systemProperties));
                assert.equal('string', data.response.hostStatus);
                assert.equal(2, data.response.gcpState);
                assert.equal(3, data.response.autoBalancedCollectorGroupId);
                assert.equal(8, data.response.autoPropsUpdatedOn);
                assert.equal(7, data.response.scanConfigId);
                assert.equal(true, Array.isArray(data.response.inheritedProperties));
                assert.equal(6, data.response.id);
                assert.equal(true, data.response.enableNetflow);
                assert.equal(4, data.response.lastDataTime);
                assert.equal('16,4,3', data.response.hostGroupIds);
                assert.equal(7, data.response.upTimeInSeconds);
                assert.equal(9, data.response.deviceType);
                assert.equal(1, data.response.currentCollectorId);
                assert.equal('string', data.response.netflowCollectorDescription);
                assert.equal(1, data.response.netflowCollectorId);
                assert.equal('string', data.response.userPermission);
                assert.equal(2, data.response.autoPropsAssignedOn);
                assert.equal(9, data.response.updatedOn);
                assert.equal('string', data.response.preferredCollectorGroupName);
                assert.equal(7, data.response.preferredCollectorGroupId);
                assert.equal(true, Array.isArray(data.response.autoProperties));
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal(5, data.response.toDeleteTimeInMs);
                assert.equal('string', data.response.collectorDescription);
                assert.equal(2, data.response.preferredCollectorId);
                assert.equal(2, data.response.lastRawdataTime);
                assert.equal('Main Collector', data.response.name);
                assert.equal(10, data.response.deletedTimeInMs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertListByDeviceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertListByDeviceId(null, null, null, devicesId, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAlertListByDeviceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetflowEndpointList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetflowEndpointList(null, null, null, devicesId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getNetflowEndpointList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetflowFlowList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetflowFlowList(null, null, null, devicesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getNetflowFlowList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDTHistoryByDeviceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDTHistoryByDeviceId(null, null, null, devicesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getSDTHistoryByDeviceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceInstanceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceInstanceList(null, null, null, devicesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceInstanceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetflowPortList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetflowPortList(null, null, null, devicesId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getNetflowPortList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSDTListByDeviceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSDTListByDeviceId(null, null, null, devicesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllSDTListByDeviceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopTalkersGraph - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopTalkersGraph(null, null, null, devicesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.missinglines));
                assert.equal('string', data.response.timeScale);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(true, Array.isArray(data.response.timestamps));
                assert.equal('object', typeof data.response.minValue);
                assert.equal(6, data.response.startTime);
                assert.equal(10, data.response.id);
                assert.equal(false, data.response.rigid);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(3, data.response.height);
                assert.equal(6, data.response.endTZOffset);
                assert.equal(false, data.response.base1024);
                assert.equal('string', data.response.dsName);
                assert.equal('object', typeof data.response.maxValue);
                assert.equal(7, data.response.displayPrio);
                assert.equal('string', data.response.timeZoneId);
                assert.equal('string', data.response.timeZone);
                assert.equal(3, data.response.startTZOffset);
                assert.equal('string', data.response.xAxisName);
                assert.equal(3, data.response.width);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.verticalLabel);
                assert.equal(8, data.response.step);
                assert.equal(3, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal(5, data.response.base);
                assert.equal('string', data.response.exportFileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getTopTalkersGraph', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnmonitoredDeviceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUnmonitoredDeviceList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getUnmonitoredDeviceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configSourcesFile = 'fakedata';
    describe('#importConfigSource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importConfigSource(configSourcesFile, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigSources', 'importConfigSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectorVersionList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCollectorVersionList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CollectorVersions', 'getCollectorVersionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const datasourcesFile = 'fakedata';
    describe('#importDataSource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importDataSource(datasourcesFile, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Datasources', 'importDataSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatasourceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDatasourceList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Datasources', 'getDatasourceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const datasourcesDsId = 555;
    describe('#getDataSourceOverviewGraphList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataSourceOverviewGraphList(datasourcesDsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Datasources', 'getDataSourceOverviewGraphList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataSourceOverviewGraphById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataSourceOverviewGraphById(datasourcesDsId, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.base1024);
                assert.equal('string', data.response.timeScale);
                assert.equal('object', typeof data.response.maxValue);
                assert.equal(6, data.response.displayPrio);
                assert.equal(true, data.response.aggregated);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.virtualDataPoints));
                assert.equal('object', typeof data.response.minValue);
                assert.equal('string', data.response.name);
                assert.equal(7, data.response.width);
                assert.equal(true, Array.isArray(data.response.dataPoints));
                assert.equal('string', data.response.verticalLabel);
                assert.equal(1, data.response.id);
                assert.equal(true, data.response.rigid);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(8, data.response.height);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Datasources', 'getDataSourceOverviewGraphById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatasourceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDatasourceById(555, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.collectorAttribute);
                assert.equal('object', typeof data.response.autoDiscoveryConfig);
                assert.equal('object', typeof data.response.eriDiscoveryConfig);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.appliesTo);
                assert.equal(true, data.response.enableAutoDiscovery);
                assert.equal('string', data.response.technology);
                assert.equal(2, data.response.version);
                assert.equal('string', data.response.tags);
                assert.equal(7, data.response.auditVersion);
                assert.equal(false, data.response.hasMultiInstances);
                assert.equal('string', data.response.collectMethod);
                assert.equal(7, data.response.eriDiscoveryInterval);
                assert.equal(false, data.response.enableEriDiscovery);
                assert.equal(9, data.response.collectInterval);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.dataPoints));
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.group);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Datasources', 'getDatasourceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const datasourcesId = 555;
    describe('#getAssociatedDeviceListByDataSourceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssociatedDeviceListByDataSourceId(datasourcesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Datasources', 'getAssociatedDeviceListByDataSourceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpdateReasonListByDataSourceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUpdateReasonListByDataSourceId(datasourcesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Datasources', 'getUpdateReasonListByDataSourceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let deviceGroupsId = 'fakedata';
    let deviceGroupsName = 'fakedata';
    const deviceGroupsAddDeviceGroupBodyParam = {
      name: 'Linux Servers'
    };
    describe('#addDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDeviceGroup(deviceGroupsAddDeviceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fullPath);
                assert.equal('Normal', data.response.groupType);
                assert.equal(8, data.response.numOfAWSDevices);
                assert.equal('Linux Servers', data.response.description);
                assert.equal('isLinux()', data.response.appliesTo);
                assert.equal(6, data.response.gcpTestResultCode);
                assert.equal(true, data.response.disableAlerting);
                assert.equal('string', data.response.awsRegionsInfo);
                assert.equal(4, data.response.createdOn);
                assert.equal(false, data.response.hasNetflowEnabledDevices);
                assert.equal(5, data.response.numOfAzureDevices);
                assert.equal('string', data.response.defaultCollectorDescription);
                assert.equal(1, data.response.defaultCollectorId);
                assert.equal('object', typeof data.response.extra);
                assert.equal(2, data.response.numOfDirectSubGroups);
                assert.equal(true, Array.isArray(data.response.subGroups));
                assert.equal(3, data.response.numOfDirectDevices);
                assert.equal(2, data.response.id);
                assert.equal('true', data.response.enableNetflow);
                assert.equal(1, data.response.azureTestResultCode);
                assert.equal(false, data.response.effectiveAlertEnabled);
                assert.equal('string', data.response.userPermission);
                assert.equal('string', data.response.gcpRegionsInfo);
                assert.equal('string', data.response.groupStatus);
                assert.equal(7, data.response.numOfGcpDevices);
                assert.equal(1, data.response.parentId);
                assert.equal(9, data.response.awsTestResultCode);
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal(8, data.response.numOfHosts);
                assert.equal('Linux Servers', data.response.name);
                assert.equal('string', data.response.azureRegionsInfo);
              } else {
                runCommonAsserts(data, error);
              }
              deviceGroupsId = data.response.id;
              deviceGroupsName = data.response.name;
              saveMockData('DeviceGroups', 'addDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsGid = 555;
    const deviceGroupsAddDeviceGroupPropertyBodyParam = {
      inheritList: [
        {
          fullpath: 'string',
          id: 2,
          type: 'string',
          value: 'string'
        }
      ],
      name: 'string',
      type: 'string',
      value: 'string'
    };
    describe('#addDeviceGroupProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDeviceGroupProperty(deviceGroupsGid, deviceGroupsAddDeviceGroupPropertyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.inheritList));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'addDeviceGroupProperty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAwsExternalId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAwsExternalId((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.externalId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getAwsExternalId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsDeviceGroupId = 555;
    describe('#getDeviceGroupDatasourceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupDatasourceList(deviceGroupsDeviceGroupId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupDatasourceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsUpdateDeviceGroupDatasourceByIdBodyParam = {
      stopMonitoring: false,
      dataSourceId: 2,
      dataSourceGroupName: 'string',
      deviceGroupId: 1,
      dataSourceDisplayName: 'string',
      disableAlerting: false,
      dataSourceName: 'string',
      dataSourceType: 'string'
    };
    describe('#updateDeviceGroupDatasourceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupDatasourceById(deviceGroupsDeviceGroupId, deviceGroupsId, deviceGroupsUpdateDeviceGroupDatasourceByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'updateDeviceGroupDatasourceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsPatchDeviceGroupDatasourceByIdBodyParam = {
      stopMonitoring: false,
      dataSourceId: 2,
      dataSourceGroupName: 'string',
      deviceGroupId: 5,
      dataSourceDisplayName: 'string',
      disableAlerting: true,
      dataSourceName: 'string',
      dataSourceType: 'string'
    };
    describe('#patchDeviceGroupDatasourceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDeviceGroupDatasourceById(deviceGroupsDeviceGroupId, deviceGroupsId, deviceGroupsPatchDeviceGroupDatasourceByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'patchDeviceGroupDatasourceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupDatasourceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupDatasourceById(deviceGroupsDeviceGroupId, deviceGroupsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.stopMonitoring);
                assert.equal(1, data.response.dataSourceId);
                assert.equal('string', data.response.dataSourceGroupName);
                assert.equal(1, data.response.deviceGroupId);
                assert.equal('string', data.response.dataSourceDisplayName);
                assert.equal(false, data.response.disableAlerting);
                assert.equal('string', data.response.dataSourceName);
                assert.equal('string', data.response.dataSourceType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupDatasourceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupPropertyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupPropertyList(deviceGroupsGid, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupPropertyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsUpdateDeviceGroupPropertyByNameBodyParam = {
      inheritList: [
        {
          fullpath: 'string',
          id: 1,
          type: 'string',
          value: 'string'
        }
      ],
      name: 'string',
      type: 'string',
      value: 'string'
    };
    describe('#updateDeviceGroupPropertyByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupPropertyByName(deviceGroupsGid, deviceGroupsName, deviceGroupsUpdateDeviceGroupPropertyByNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'updateDeviceGroupPropertyByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsPatchDeviceGroupPropertyByNameBodyParam = {
      inheritList: [
        {
          fullpath: 'string',
          id: 10,
          type: 'string',
          value: 'string'
        }
      ],
      name: 'string',
      type: 'string',
      value: 'string'
    };
    describe('#patchDeviceGroupPropertyByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDeviceGroupPropertyByName(deviceGroupsGid, deviceGroupsName, deviceGroupsPatchDeviceGroupPropertyByNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'patchDeviceGroupPropertyByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupPropertyByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupPropertyByName(deviceGroupsGid, deviceGroupsName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.inheritList));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupPropertyByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsUpdateDeviceGroupByIdBodyParam = {
      name: 'Linux Servers'
    };
    describe('#updateDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupById(deviceGroupsId, deviceGroupsUpdateDeviceGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'updateDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsPatchDeviceGroupByIdBodyParam = {
      name: 'Linux Servers'
    };
    describe('#patchDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDeviceGroupById(deviceGroupsId, deviceGroupsPatchDeviceGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'patchDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupById(deviceGroupsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fullPath);
                assert.equal('Normal', data.response.groupType);
                assert.equal(7, data.response.numOfAWSDevices);
                assert.equal('Linux Servers', data.response.description);
                assert.equal('isLinux()', data.response.appliesTo);
                assert.equal(7, data.response.gcpTestResultCode);
                assert.equal(true, data.response.disableAlerting);
                assert.equal('string', data.response.awsRegionsInfo);
                assert.equal(6, data.response.createdOn);
                assert.equal(false, data.response.hasNetflowEnabledDevices);
                assert.equal(9, data.response.numOfAzureDevices);
                assert.equal('string', data.response.defaultCollectorDescription);
                assert.equal(1, data.response.defaultCollectorId);
                assert.equal('object', typeof data.response.extra);
                assert.equal(10, data.response.numOfDirectSubGroups);
                assert.equal(true, Array.isArray(data.response.subGroups));
                assert.equal(7, data.response.numOfDirectDevices);
                assert.equal(1, data.response.id);
                assert.equal('true', data.response.enableNetflow);
                assert.equal(10, data.response.azureTestResultCode);
                assert.equal(false, data.response.effectiveAlertEnabled);
                assert.equal('string', data.response.userPermission);
                assert.equal('string', data.response.gcpRegionsInfo);
                assert.equal('string', data.response.groupStatus);
                assert.equal(5, data.response.numOfGcpDevices);
                assert.equal(1, data.response.parentId);
                assert.equal(9, data.response.awsTestResultCode);
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal(1, data.response.numOfHosts);
                assert.equal('Linux Servers', data.response.name);
                assert.equal('string', data.response.azureRegionsInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertListByDeviceGroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertListByDeviceGroupId(deviceGroupsId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getAlertListByDeviceGroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImmediateDeviceListByDeviceGroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImmediateDeviceListByDeviceGroupId(deviceGroupsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getImmediateDeviceListByDeviceGroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDTHistoryByDeviceGroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDTHistoryByDeviceGroupId(deviceGroupsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getSDTHistoryByDeviceGroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupSDTList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupSDTList(deviceGroupsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupSDTList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let datasourceInstancesDeviceId = 555;
    const datasourceInstancesDeviceDsId = 555;
    let datasourceInstancesId = 'fakedata';
    const datasourceInstancesAddDeviceDatasourceInstanceGroupBodyParam = {
      name: 'Ping Checks at HQ'
    };
    describe('#addDeviceDatasourceInstanceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDeviceDatasourceInstanceGroup(datasourceInstancesDeviceId, datasourceInstancesDeviceDsId, datasourceInstancesAddDeviceDatasourceInstanceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.deviceDataSourceId);
                assert.equal('Ping Checks at HQ', data.response.name);
                assert.equal('Test the availability of HQ', data.response.description);
                assert.equal(9, data.response.createOn);
                assert.equal(5, data.response.id);
                assert.equal(10, data.response.deviceId);
                assert.equal('string', data.response.deviceDisplayName);
              } else {
                runCommonAsserts(data, error);
              }
              datasourceInstancesDeviceId = data.response.id;
              datasourceInstancesId = data.response.id;
              saveMockData('DatasourceInstances', 'addDeviceDatasourceInstanceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let datasourceInstancesHdsId = 555;
    let datasourceInstancesInstanceId = 555;
    const datasourceInstancesAddDeviceDatasourceInstanceBodyParam = {
      displayName: 'Ping',
      wildValue: '1'
    };
    describe('#addDeviceDatasourceInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDeviceDatasourceInstance(datasourceInstancesDeviceId, datasourceInstancesHdsId, datasourceInstancesAddDeviceDatasourceInstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.stopMonitoring);
                assert.equal(7, data.response.deviceDataSourceId);
                assert.equal('Ping', data.response.displayName);
                assert.equal('1', data.response.wildValue2);
                assert.equal(211, data.response.groupId);
                assert.equal('Ping Test', data.response.description);
                assert.equal(true, data.response.disableAlerting);
                assert.equal(10, data.response.deviceId);
                assert.equal('string', data.response.deviceDisplayName);
                assert.equal(true, Array.isArray(data.response.systemProperties));
                assert.equal(true, Array.isArray(data.response.autoProperties));
                assert.equal(5, data.response.dataSourceId);
                assert.equal('string', data.response.groupName);
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal(true, data.response.lockDescription);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.id);
                assert.equal('1', data.response.wildValue);
                assert.equal('string', data.response.dataSourceType);
              } else {
                runCommonAsserts(data, error);
              }
              datasourceInstancesHdsId = data.response.id;
              datasourceInstancesInstanceId = data.response.id;
              saveMockData('DatasourceInstances', 'addDeviceDatasourceInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceInstanceGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceGroupList(datasourceInstancesDeviceId, datasourceInstancesDeviceDsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'getDeviceDatasourceInstanceGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const datasourceInstancesUpdateDeviceDatasourceInstanceGroupByIdBodyParam = {
      name: 'Ping Checks at HQ'
    };
    describe('#updateDeviceDatasourceInstanceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceDatasourceInstanceGroupById(datasourceInstancesDeviceId, datasourceInstancesDeviceDsId, datasourceInstancesId, datasourceInstancesUpdateDeviceDatasourceInstanceGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'updateDeviceDatasourceInstanceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const datasourceInstancesPatchDeviceDatasourceInstanceGroupByIdBodyParam = {
      name: 'Ping Checks at HQ'
    };
    describe('#patchDeviceDatasourceInstanceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDeviceDatasourceInstanceGroupById(datasourceInstancesDeviceId, datasourceInstancesDeviceDsId, datasourceInstancesId, datasourceInstancesPatchDeviceDatasourceInstanceGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'patchDeviceDatasourceInstanceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceInstanceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceGroupById(datasourceInstancesDeviceId, datasourceInstancesDeviceDsId, datasourceInstancesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.deviceDataSourceId);
                assert.equal('Ping Checks at HQ', data.response.name);
                assert.equal('Test the availability of HQ', data.response.description);
                assert.equal(10, data.response.createOn);
                assert.equal(4, data.response.id);
                assert.equal(8, data.response.deviceId);
                assert.equal('string', data.response.deviceDisplayName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'getDeviceDatasourceInstanceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceInstanceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceList(datasourceInstancesDeviceId, datasourceInstancesHdsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'getDeviceDatasourceInstanceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const datasourceInstancesUpdateDeviceDatasourceInstanceByIdBodyParam = {
      displayName: 'Ping',
      wildValue: '1'
    };
    describe('#updateDeviceDatasourceInstanceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceDatasourceInstanceById(datasourceInstancesDeviceId, datasourceInstancesHdsId, datasourceInstancesId, datasourceInstancesUpdateDeviceDatasourceInstanceByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'updateDeviceDatasourceInstanceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const datasourceInstancesPatchDeviceDatasourceInstanceByIdBodyParam = {
      displayName: 'Ping',
      wildValue: '1'
    };
    describe('#patchDeviceDatasourceInstanceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDeviceDatasourceInstanceById(datasourceInstancesDeviceId, datasourceInstancesHdsId, datasourceInstancesId, datasourceInstancesPatchDeviceDatasourceInstanceByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'patchDeviceDatasourceInstanceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceInstanceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceById(datasourceInstancesDeviceId, datasourceInstancesHdsId, datasourceInstancesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.stopMonitoring);
                assert.equal(9, data.response.deviceDataSourceId);
                assert.equal('Ping', data.response.displayName);
                assert.equal('1', data.response.wildValue2);
                assert.equal(211, data.response.groupId);
                assert.equal('Ping Test', data.response.description);
                assert.equal(true, data.response.disableAlerting);
                assert.equal(3, data.response.deviceId);
                assert.equal('string', data.response.deviceDisplayName);
                assert.equal(true, Array.isArray(data.response.systemProperties));
                assert.equal(true, Array.isArray(data.response.autoProperties));
                assert.equal(7, data.response.dataSourceId);
                assert.equal('string', data.response.groupName);
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal(true, data.response.lockDescription);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.id);
                assert.equal('1', data.response.wildValue);
                assert.equal('string', data.response.dataSourceType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'getDeviceDatasourceInstanceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceInstanceSDTHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceSDTHistory(datasourceInstancesDeviceId, datasourceInstancesHdsId, datasourceInstancesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'getDeviceDatasourceInstanceSDTHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceConfigSourceConfigList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceConfigSourceConfigList(datasourceInstancesDeviceId, datasourceInstancesHdsId, datasourceInstancesInstanceId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'getDeviceConfigSourceConfigList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#collectDeviceConfigSourceConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.collectDeviceConfigSourceConfig(datasourceInstancesDeviceId, datasourceInstancesHdsId, datasourceInstancesInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'collectDeviceConfigSourceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventSourcesFile = 'fakedata';
    describe('#importEventSource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importEventSource(eventSourcesFile, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventSources', 'importEventSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventSourceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEventSourceList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventSources', 'getEventSourceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsId = 'fakedata';
    const alertsAckAlertByIdBodyParam = {
      ackComment: 'looking into this alert'
    };
    describe('#ackAlertById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ackAlertById(alertsAckAlertByIdBodyParam, alertsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'ackAlertById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsAddAlertNoteByIdBodyParam = {
      ackComment: 'looking into this alert'
    };
    describe('#addAlertNoteById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAlertNoteById(alertsAddAlertNoteByIdBodyParam, alertsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'addAlertNoteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlertList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertById(alertsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.resourceId);
                assert.equal('string', data.response.instanceName);
                assert.equal(2, data.response.monitorObjectId);
                assert.equal(5, data.response.endEpoch);
                assert.equal('string', data.response.rule);
                assert.equal('string', data.response.threshold);
                assert.equal('string', data.response.type);
                assert.equal(3, data.response.startEpoch);
                assert.equal('string', data.response.enableAnomalyAlertGeneration);
                assert.equal('string', data.response.internalId);
                assert.equal('string', data.response.ackComment);
                assert.equal('string', data.response.monitorObjectName);
                assert.equal('string', data.response.dataPointName);
                assert.equal(3, data.response.instanceId);
                assert.equal(7, data.response.dataPointId);
                assert.equal(6, data.response.nextRecipient);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.detailMessage);
                assert.equal(7, data.response.ruleId);
                assert.equal('string', data.response.alertValue);
                assert.equal('string', data.response.ackedBy);
                assert.equal(2, data.response.severity);
                assert.equal(true, data.response.sdted);
                assert.equal(6, data.response.ackedEpoch);
                assert.equal('string', data.response.chain);
                assert.equal('object', typeof data.response.SDT);
                assert.equal(5, data.response.subChainId);
                assert.equal('string', data.response.enableAnomalyAlertSuppression);
                assert.equal('string', data.response.receivedList);
                assert.equal('string', data.response.monitorObjectType);
                assert.equal('object', typeof data.response.customColumns);
                assert.equal(false, data.response.acked);
                assert.equal('string', data.response.resourceTemplateType);
                assert.equal('string', data.response.clearValue);
                assert.equal('string', data.response.instanceDescription);
                assert.equal('object', typeof data.response.monitorObjectGroups);
                assert.equal(5, data.response.chainId);
                assert.equal(10, data.response.resourceTemplateId);
                assert.equal(true, data.response.cleared);
                assert.equal('string', data.response.resourceTemplateName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlertById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let opsNotesId = 'fakedata';
    const opsNotesAddOpsNoteBodyParam = {
      note: 'software update from 1.0.0 to 1.2.4'
    };
    describe('#addOpsNote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addOpsNote(opsNotesAddOpsNoteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('software update from 1.0.0 to 1.2.4', data.response.note);
                assert.equal('string', data.response.createdBy);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('string', data.response.id);
                assert.equal(1488826440, data.response.happenOnInSec);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              opsNotesId = data.response.id;
              saveMockData('OpsNotes', 'addOpsNote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsNoteList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsNoteList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpsNotes', 'getOpsNoteList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const opsNotesUpdateOpsNoteByIdBodyParam = {
      note: 'software update from 1.0.0 to 1.2.4'
    };
    describe('#updateOpsNoteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOpsNoteById(opsNotesId, opsNotesUpdateOpsNoteByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpsNotes', 'updateOpsNoteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const opsNotesPatchOpsNoteByIdBodyParam = {
      note: 'software update from 1.0.0 to 1.2.4'
    };
    describe('#patchOpsNoteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchOpsNoteById(opsNotesId, opsNotesPatchOpsNoteByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpsNotes', 'patchOpsNoteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpsNoteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpsNoteById(opsNotesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('software update from 1.0.0 to 1.2.4', data.response.note);
                assert.equal('string', data.response.createdBy);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('string', data.response.id);
                assert.equal(1488826440, data.response.happenOnInSec);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpsNotes', 'getOpsNoteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let websitesId = 'fakedata';
    let websitesGraphName = 'fakedata';
    const websitesAddWebsiteBodyParam = {
      testLocation: null,
      type: 'webcheck',
      name: 'Ebay'
    };
    describe('#addWebsite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addWebsite(websitesAddWebsiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.template);
                assert.equal(1, data.response.groupId);
                assert.equal('warn', data.response.overallAlertLevel);
                assert.equal(5, data.response.pollingInterval);
                assert.equal('Monitor Ebay site response times', data.response.description);
                assert.equal(false, data.response.disableAlerting);
                assert.equal('webcheck', data.response.type);
                assert.equal(5, data.response.lastUpdated);
                assert.equal(true, data.response.stopMonitoringByFolder);
                assert.equal(8, data.response.id);
                assert.equal(false, data.response.stopMonitoring);
                assert.equal('string', data.response.userPermission);
                assert.equal(true, data.response.individualSmAlertEnable);
                assert.equal(true, Array.isArray(data.response.checkpoints));
                assert.equal(1, data.response.transition);
                assert.equal(4, data.response.globalSmAlertCond);
                assert.equal(false, data.response.isInternal);
                assert.equal(true, Array.isArray(data.response.collectors));
                assert.equal('Ebay', data.response.name);
                assert.equal(false, data.response.useDefaultLocationSetting);
                assert.equal(true, data.response.useDefaultAlertSetting);
                assert.equal('warn', data.response.individualAlertLevel);
                assert.equal(true, Array.isArray(data.response.properties));
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              websitesId = data.response.id;
              websitesGraphName = data.response.name;
              saveMockData('Websites', 'addWebsite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteMonitorCheckPointList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteMonitorCheckPointList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'getSiteMonitorCheckPointList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebsiteList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'getWebsiteList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const websitesUpdateWebsiteByIdBodyParam = {
      testLocation: null,
      type: 'webcheck',
      name: 'Ebay'
    };
    describe('#updateWebsiteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWebsiteById(websitesId, websitesUpdateWebsiteByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'updateWebsiteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const websitesPatchWebsiteByIdBodyParam = {
      testLocation: null,
      type: 'webcheck',
      name: 'Ebay'
    };
    describe('#patchWebsiteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchWebsiteById(websitesId, websitesPatchWebsiteByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'patchWebsiteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebsiteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteById(websitesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.template);
                assert.equal(1, data.response.groupId);
                assert.equal('warn', data.response.overallAlertLevel);
                assert.equal(5, data.response.pollingInterval);
                assert.equal('Monitor Ebay site response times', data.response.description);
                assert.equal(false, data.response.disableAlerting);
                assert.equal('webcheck', data.response.type);
                assert.equal(1, data.response.lastUpdated);
                assert.equal(true, data.response.stopMonitoringByFolder);
                assert.equal(6, data.response.id);
                assert.equal(false, data.response.stopMonitoring);
                assert.equal('string', data.response.userPermission);
                assert.equal(true, data.response.individualSmAlertEnable);
                assert.equal(true, Array.isArray(data.response.checkpoints));
                assert.equal(1, data.response.transition);
                assert.equal(5, data.response.globalSmAlertCond);
                assert.equal(false, data.response.isInternal);
                assert.equal(true, Array.isArray(data.response.collectors));
                assert.equal('Ebay', data.response.name);
                assert.equal(true, data.response.useDefaultLocationSetting);
                assert.equal(true, data.response.useDefaultAlertSetting);
                assert.equal('warn', data.response.individualAlertLevel);
                assert.equal(true, Array.isArray(data.response.properties));
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'getWebsiteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebsiteAlertListByWebsiteId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteAlertListByWebsiteId(websitesId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'getWebsiteAlertListByWebsiteId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebsiteDataByGraphName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteDataByGraphName(websitesId, null, null, websitesGraphName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.missinglines));
                assert.equal('string', data.response.timeScale);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(true, Array.isArray(data.response.timestamps));
                assert.equal('object', typeof data.response.minValue);
                assert.equal(9, data.response.startTime);
                assert.equal(4, data.response.id);
                assert.equal(true, data.response.rigid);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(10, data.response.height);
                assert.equal(2, data.response.endTZOffset);
                assert.equal(true, data.response.base1024);
                assert.equal('string', data.response.dsName);
                assert.equal('object', typeof data.response.maxValue);
                assert.equal(9, data.response.displayPrio);
                assert.equal('string', data.response.timeZoneId);
                assert.equal('string', data.response.timeZone);
                assert.equal(2, data.response.startTZOffset);
                assert.equal('string', data.response.xAxisName);
                assert.equal(5, data.response.width);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.verticalLabel);
                assert.equal(8, data.response.step);
                assert.equal(1, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal(1, data.response.base);
                assert.equal('string', data.response.exportFileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'getWebsiteDataByGraphName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDTHistoryByWebsiteId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDTHistoryByWebsiteId(websitesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'getSDTHistoryByWebsiteId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebsitePropertyListByWebsiteId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsitePropertyListByWebsiteId(websitesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'getWebsitePropertyListByWebsiteId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebsiteSDTListByWebsiteId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteSDTListByWebsiteId(websitesId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'getWebsiteSDTListByWebsiteId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataFetchDeviceInstancesDataBodyParam = {
      instanceIds: '1,2,3,4,5'
    };
    describe('#fetchDeviceInstancesData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.fetchDeviceInstancesData(dataFetchDeviceInstancesDataBodyParam, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'fetchDeviceInstancesData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataId = 555;
    describe('#getWidgetDataById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWidgetDataById(null, null, null, dataId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.title);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'getWidgetDataById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataInstanceId = 555;
    const dataGraphId = 555;
    describe('#getDeviceInstanceGraphDataOnlyByInstanceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceInstanceGraphDataOnlyByInstanceId(dataInstanceId, dataGraphId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.missinglines));
                assert.equal('string', data.response.timeScale);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(true, Array.isArray(data.response.timestamps));
                assert.equal('object', typeof data.response.minValue);
                assert.equal(2, data.response.startTime);
                assert.equal(10, data.response.id);
                assert.equal(false, data.response.rigid);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(2, data.response.height);
                assert.equal(8, data.response.endTZOffset);
                assert.equal(true, data.response.base1024);
                assert.equal('string', data.response.dsName);
                assert.equal('object', typeof data.response.maxValue);
                assert.equal(8, data.response.displayPrio);
                assert.equal('string', data.response.timeZoneId);
                assert.equal('string', data.response.timeZone);
                assert.equal(7, data.response.startTZOffset);
                assert.equal('string', data.response.xAxisName);
                assert.equal(7, data.response.width);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.verticalLabel);
                assert.equal(6, data.response.step);
                assert.equal(1, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal(8, data.response.base);
                assert.equal('string', data.response.exportFileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'getDeviceInstanceGraphDataOnlyByInstanceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataDeviceId = 555;
    const dataDeviceDsId = 555;
    const dataDsigId = 555;
    const dataOgraphId = 555;
    describe('#getDeviceDatasourceInstanceGroupOverviewGraphData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceGroupOverviewGraphData(dataDeviceId, dataDeviceDsId, dataDsigId, null, null, dataOgraphId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.missinglines));
                assert.equal('string', data.response.timeScale);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(true, Array.isArray(data.response.timestamps));
                assert.equal('object', typeof data.response.minValue);
                assert.equal(5, data.response.startTime);
                assert.equal(1, data.response.id);
                assert.equal(false, data.response.rigid);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(7, data.response.height);
                assert.equal(5, data.response.endTZOffset);
                assert.equal(false, data.response.base1024);
                assert.equal('string', data.response.dsName);
                assert.equal('object', typeof data.response.maxValue);
                assert.equal(3, data.response.displayPrio);
                assert.equal('string', data.response.timeZoneId);
                assert.equal('string', data.response.timeZone);
                assert.equal(7, data.response.startTZOffset);
                assert.equal('string', data.response.xAxisName);
                assert.equal(8, data.response.width);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.verticalLabel);
                assert.equal(9, data.response.step);
                assert.equal(3, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal(8, data.response.base);
                assert.equal('string', data.response.exportFileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'getDeviceDatasourceInstanceGroupOverviewGraphData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataHdsId = 555;
    describe('#getDeviceDatasourceInstanceData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceData(dataDeviceId, dataHdsId, dataId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.values));
                assert.equal(true, Array.isArray(data.response.time));
                assert.equal('string', data.response.nextPageParams);
                assert.equal('string', data.response.dataSourceName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'getDeviceDatasourceInstanceData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceInstanceGraphData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceGraphData(dataDeviceId, dataHdsId, dataId, null, null, dataGraphId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.missinglines));
                assert.equal('string', data.response.timeScale);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(true, Array.isArray(data.response.timestamps));
                assert.equal('object', typeof data.response.minValue);
                assert.equal(1, data.response.startTime);
                assert.equal(6, data.response.id);
                assert.equal(false, data.response.rigid);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(10, data.response.height);
                assert.equal(6, data.response.endTZOffset);
                assert.equal(false, data.response.base1024);
                assert.equal('string', data.response.dsName);
                assert.equal('object', typeof data.response.maxValue);
                assert.equal(1, data.response.displayPrio);
                assert.equal('string', data.response.timeZoneId);
                assert.equal('string', data.response.timeZone);
                assert.equal(8, data.response.startTZOffset);
                assert.equal('string', data.response.xAxisName);
                assert.equal(6, data.response.width);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.verticalLabel);
                assert.equal(9, data.response.step);
                assert.equal(5, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal(3, data.response.base);
                assert.equal('string', data.response.exportFileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'getDeviceDatasourceInstanceGraphData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceConfigSourceConfigById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceConfigSourceConfigById(dataDeviceId, dataHdsId, dataInstanceId, dataId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.instanceName);
                assert.equal(3, data.response.deviceDataSourceId);
                assert.equal(2, data.response.configStatus);
                assert.equal(9, data.response.version);
                assert.equal(8, data.response.deviceId);
                assert.equal(true, Array.isArray(data.response.deltaConfig));
                assert.equal('string', data.response.deviceDisplayName);
                assert.equal('string', data.response.dataSourceName);
                assert.equal(true, Array.isArray(data.response.alerts));
                assert.equal(1, data.response.dataSourceId);
                assert.equal(1, data.response.instanceId);
                assert.equal('string', data.response.configErrMsg);
                assert.equal('string', data.response.changeStatus);
                assert.equal('string', data.response.id);
                assert.equal(10, data.response.pollTimestamp);
                assert.equal('string', data.response.config);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'getDeviceConfigSourceConfigById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataSrvId = 555;
    const dataCheckId = 555;
    describe('#getWebsiteCheckpointDataById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteCheckpointDataById(dataSrvId, dataCheckId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.values));
                assert.equal(true, Array.isArray(data.response.time));
                assert.equal('string', data.response.nextPageParams);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'getWebsiteCheckpointDataById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataWebsiteId = 555;
    const dataCheckpointId = 555;
    const dataGraphName = 'fakedata';
    describe('#getWebsiteGraphData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteGraphData(dataWebsiteId, dataCheckpointId, null, null, dataGraphName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.missinglines));
                assert.equal('string', data.response.timeScale);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(true, Array.isArray(data.response.timestamps));
                assert.equal('object', typeof data.response.minValue);
                assert.equal(5, data.response.startTime);
                assert.equal(6, data.response.id);
                assert.equal(true, data.response.rigid);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(10, data.response.height);
                assert.equal(8, data.response.endTZOffset);
                assert.equal(false, data.response.base1024);
                assert.equal('string', data.response.dsName);
                assert.equal('object', typeof data.response.maxValue);
                assert.equal(6, data.response.displayPrio);
                assert.equal('string', data.response.timeZoneId);
                assert.equal('string', data.response.timeZone);
                assert.equal(5, data.response.startTZOffset);
                assert.equal('string', data.response.xAxisName);
                assert.equal(7, data.response.width);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.verticalLabel);
                assert.equal(8, data.response.step);
                assert.equal(7, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal(7, data.response.base);
                assert.equal('string', data.response.exportFileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Data', 'getWebsiteGraphData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuditLogList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditLogs', 'getAuditLogList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuditLogById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.happenedOnLocal);
                assert.equal('string', data.response.ip);
                assert.equal(10, data.response.happenedOn);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditLogs', 'getAuditLogById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let escalationChainsId = 'fakedata';
    const escalationChainsAddEscalationChainBodyParam = {
      destinations: [
        {}
      ],
      name: 'NOC Team'
    };
    describe('#addEscalationChain - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addEscalationChain(escalationChainsAddEscalationChainBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.inAlerting);
                assert.equal(40, data.response.throttlingAlerts);
                assert.equal(true, data.response.enableThrottling);
                assert.equal(true, Array.isArray(data.response.destinations));
                assert.equal('NOC Team', data.response.name);
                assert.equal('For alerts escalated to the NOC Team', data.response.description);
                assert.equal(4, data.response.id);
                assert.equal(true, Array.isArray(data.response.ccDestinations));
                assert.equal(30, data.response.throttlingPeriod);
              } else {
                runCommonAsserts(data, error);
              }
              escalationChainsId = data.response.id;
              saveMockData('EscalationChains', 'addEscalationChain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEscalationChainList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEscalationChainList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationChains', 'getEscalationChainList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const escalationChainsUpdateEscalationChainByIdBodyParam = {
      destinations: [
        {}
      ],
      name: 'NOC Team'
    };
    describe('#updateEscalationChainById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateEscalationChainById(escalationChainsId, escalationChainsUpdateEscalationChainByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationChains', 'updateEscalationChainById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const escalationChainsPatchEscalationChainByIdBodyParam = {
      destinations: [
        {}
      ],
      name: 'NOC Team'
    };
    describe('#patchEscalationChainById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchEscalationChainById(escalationChainsId, escalationChainsPatchEscalationChainByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationChains', 'patchEscalationChainById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEscalationChainById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEscalationChainById(escalationChainsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.inAlerting);
                assert.equal(40, data.response.throttlingAlerts);
                assert.equal(true, data.response.enableThrottling);
                assert.equal(true, Array.isArray(data.response.destinations));
                assert.equal('NOC Team', data.response.name);
                assert.equal('For alerts escalated to the NOC Team', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal(true, Array.isArray(data.response.ccDestinations));
                assert.equal(30, data.response.throttlingPeriod);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationChains', 'getEscalationChainById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let appliesToFunctionsId = 'fakedata';
    const appliesToFunctionsAddAppliesToFunctionBodyParam = {
      code: 'string',
      name: 'string'
    };
    describe('#addAppliesToFunction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAppliesToFunction(appliesToFunctionsAddAppliesToFunctionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.code);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              appliesToFunctionsId = data.response.id;
              saveMockData('AppliesToFunctions', 'addAppliesToFunction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliesToFunctionList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliesToFunctionList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliesToFunctions', 'getAppliesToFunctionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appliesToFunctionsUpdateAppliesToFunctionBodyParam = {
      code: 'string',
      name: 'string'
    };
    describe('#updateAppliesToFunction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAppliesToFunction(appliesToFunctionsUpdateAppliesToFunctionBodyParam, appliesToFunctionsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliesToFunctions', 'updateAppliesToFunction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appliesToFunctionsPatchAppliesToFunctionBodyParam = {
      code: 'string',
      name: 'string'
    };
    describe('#patchAppliesToFunction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAppliesToFunction(appliesToFunctionsPatchAppliesToFunctionBodyParam, appliesToFunctionsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliesToFunctions', 'patchAppliesToFunction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliesToFunctionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliesToFunctionById(appliesToFunctionsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.code);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(5, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliesToFunctions', 'getAppliesToFunctionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let reportId = 'fakedata';
    const reportAddReportBodyParam = {
      type: 'Alert',
      name: 'Daily Alerts Report'
    };
    describe('#addReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addReport(reportAddReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.lastmodifyUserId);
                assert.equal('email', data.response.delivery);
                assert.equal('string', data.response.userPermission);
                assert.equal(5, data.response.lastGenerateOn);
                assert.equal(8, data.response.reportLinkNum);
                assert.equal(5, data.response.groupId);
                assert.equal('HTML', data.response.format);
                assert.equal('This is a daily alerts report', data.response.description);
                assert.equal(8, data.response.lastGenerateSize);
                assert.equal(10, data.response.customReportTypeId);
                assert.equal('Alert', data.response.type);
                assert.equal(3, data.response.lastGeneratePages);
                assert.equal('0 7 * * 1', data.response.schedule);
                assert.equal(true, Array.isArray(data.response.recipients));
                assert.equal('string', data.response.customReportTypeName);
                assert.equal('Daily Alerts Report', data.response.name);
                assert.equal(true, data.response.enableViewAsOtherUser);
                assert.equal('string', data.response.lastmodifyUserName);
                assert.equal(2, data.response.id);
                assert.equal('America/Los_Angeles', data.response.scheduleTimezone);
              } else {
                runCommonAsserts(data, error);
              }
              reportId = data.response.id;
              saveMockData('Report', 'addReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportGenerateReportByIdBodyParam = {
      withAdminId: 2,
      reportId: 998,
      receiveEmails: 'bob@logicmonitor.com'
    };
    describe('#generateReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateReportById(reportGenerateReportByIdBodyParam, reportId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.reportId);
                assert.equal(7, data.response.taskId);
                assert.equal('string', data.response.resulturl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Report', 'generateReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Report', 'getReportList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportUpdateReportByIdBodyParam = {
      type: 'Alert',
      name: 'Daily Alerts Report'
    };
    describe('#updateReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReportById(reportId, reportUpdateReportByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Report', 'updateReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportPatchReportByIdBodyParam = {
      type: 'Alert',
      name: 'Daily Alerts Report'
    };
    describe('#patchReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchReportById(reportId, reportPatchReportByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Report', 'patchReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportById(reportId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.lastmodifyUserId);
                assert.equal('email', data.response.delivery);
                assert.equal('string', data.response.userPermission);
                assert.equal(9, data.response.lastGenerateOn);
                assert.equal(8, data.response.reportLinkNum);
                assert.equal(5, data.response.groupId);
                assert.equal('HTML', data.response.format);
                assert.equal('This is a daily alerts report', data.response.description);
                assert.equal(10, data.response.lastGenerateSize);
                assert.equal(9, data.response.customReportTypeId);
                assert.equal('Alert', data.response.type);
                assert.equal(5, data.response.lastGeneratePages);
                assert.equal('0 7 * * 1', data.response.schedule);
                assert.equal(true, Array.isArray(data.response.recipients));
                assert.equal('string', data.response.customReportTypeName);
                assert.equal('Daily Alerts Report', data.response.name);
                assert.equal(false, data.response.enableViewAsOtherUser);
                assert.equal('string', data.response.lastmodifyUserName);
                assert.equal(4, data.response.id);
                assert.equal('America/Los_Angeles', data.response.scheduleTimezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Report', 'getReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let collectorsId = 'fakedata';
    let collectorsCollectorId = 555;
    const collectorsAddCollectorBodyParam = {
      userChangeOn: 2,
      confVersion: 'string',
      updatedOnLocal: 'string',
      hostname: 'string',
      numberOfInstances: 3,
      id: 9,
      ea: false,
      watchdogUpdatedOnLocal: 'string',
      hasFailOverDevice: false,
      collectorGroupName: 'string',
      netscanVersion: 'string',
      inSDT: false,
      updatedOn: 3,
      automaticUpgradeInfo: null,
      numberOfHosts: 9,
      collectorConf: 'string',
      sbproxyConf: 'string',
      userVisibleWebsitesNum: 9,
      lastSentNotificationOnLocal: 'string',
      customProperties: [
        {
          name: 'addr',
          value: '127.0.0.1'
        }
      ],
      predefinedConfig: {},
      onetimeUpgradeInfo: null,
      wrapperConf: 'string',
      clearSent: false,
      status: 2,
      backupAgentId: 75,
      specifiedCollectorDeviceGroupId: 1,
      escalatingChainId: 80,
      collectorSize: 'string',
      ackedOnLocal: 'string',
      watchdogConf: 'string',
      description: 'Linux Collector',
      createdOn: 1,
      platform: 'string',
      isDown: false,
      userVisibleHostsNum: 2,
      canDowngrade: true,
      ackComment: 'string',
      nextUpgradeInfo: null,
      suppressAlertClear: true,
      nextRecipient: 5,
      ackedOn: 9,
      userChangeOnLocal: 'string',
      numberOfWebsites: 6,
      collectorDeviceId: 2,
      ackedBy: 'string',
      userPermission: 'string',
      needAutoCreateCollectorDevice: 'true',
      watchdogUpdatedOn: 8,
      canDowngradeReason: 'string',
      lastSentNotificationOn: 2,
      acked: false,
      onetimeDowngradeInfo: null,
      websiteConf: 'string',
      upTime: 2,
      build: 'string',
      previousVersion: 'string',
      collectorGroupId: 10,
      createdOnLocal: 'string',
      enableFailBack: 'true',
      resendIval: 15,
      arch: 'string',
      enableFailOverOnCollectorDevice: true
    };
    describe('#addCollector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCollector(collectorsAddCollectorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.userChangeOn);
                assert.equal('string', data.response.confVersion);
                assert.equal('string', data.response.updatedOnLocal);
                assert.equal('string', data.response.hostname);
                assert.equal(6, data.response.numberOfInstances);
                assert.equal(2, data.response.id);
                assert.equal(false, data.response.ea);
                assert.equal('string', data.response.watchdogUpdatedOnLocal);
                assert.equal(false, data.response.hasFailOverDevice);
                assert.equal('string', data.response.collectorGroupName);
                assert.equal('string', data.response.netscanVersion);
                assert.equal(false, data.response.inSDT);
                assert.equal(8, data.response.updatedOn);
                assert.equal(10, data.response.numberOfHosts);
                assert.equal('string', data.response.collectorConf);
                assert.equal('string', data.response.sbproxyConf);
                assert.equal(4, data.response.userVisibleWebsitesNum);
                assert.equal('string', data.response.lastSentNotificationOnLocal);
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal('object', typeof data.response.predefinedConfig);
                assert.equal('string', data.response.wrapperConf);
                assert.equal(true, data.response.clearSent);
                assert.equal(8, data.response.status);
                assert.equal(75, data.response.backupAgentId);
                assert.equal(7, data.response.specifiedCollectorDeviceGroupId);
                assert.equal(80, data.response.escalatingChainId);
                assert.equal('string', data.response.collectorSize);
                assert.equal('string', data.response.ackedOnLocal);
                assert.equal('string', data.response.watchdogConf);
                assert.equal('Linux Collector', data.response.description);
                assert.equal(2, data.response.createdOn);
                assert.equal('string', data.response.platform);
                assert.equal(true, data.response.isDown);
                assert.equal(3, data.response.userVisibleHostsNum);
                assert.equal(false, data.response.canDowngrade);
                assert.equal('string', data.response.ackComment);
                assert.equal(true, data.response.suppressAlertClear);
                assert.equal(8, data.response.nextRecipient);
                assert.equal(6, data.response.ackedOn);
                assert.equal('string', data.response.userChangeOnLocal);
                assert.equal(2, data.response.numberOfWebsites);
                assert.equal(3, data.response.collectorDeviceId);
                assert.equal('string', data.response.ackedBy);
                assert.equal('string', data.response.userPermission);
                assert.equal('true', data.response.needAutoCreateCollectorDevice);
                assert.equal(1, data.response.watchdogUpdatedOn);
                assert.equal('string', data.response.canDowngradeReason);
                assert.equal(9, data.response.lastSentNotificationOn);
                assert.equal(false, data.response.acked);
                assert.equal('string', data.response.websiteConf);
                assert.equal(10, data.response.upTime);
                assert.equal('string', data.response.build);
                assert.equal('string', data.response.previousVersion);
                assert.equal(10, data.response.collectorGroupId);
                assert.equal('string', data.response.createdOnLocal);
                assert.equal('true', data.response.enableFailBack);
                assert.equal(15, data.response.resendIval);
                assert.equal('string', data.response.arch);
                assert.equal(true, data.response.enableFailOverOnCollectorDevice);
              } else {
                runCommonAsserts(data, error);
              }
              collectorsId = data.response.id;
              collectorsCollectorId = data.response.id;
              saveMockData('Collectors', 'addCollector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectorsAckCollectorDownAlertByIdBodyParam = {
      comment: 'Collector Down Acknowledged'
    };
    describe('#ackCollectorDownAlertById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ackCollectorDownAlertById(collectorsId, collectorsAckCollectorDownAlertByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collectors', 'ackCollectorDownAlertById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectorList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCollectorList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collectors', 'getCollectorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectorsOsAndArch = 'fakedata';
    describe('#getCollectorInstaller - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCollectorInstaller(collectorsCollectorId, collectorsOsAndArch, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collectors', 'getCollectorInstaller', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectorsUpdateCollectorByIdBodyParam = {
      userChangeOn: 9,
      confVersion: 'string',
      updatedOnLocal: 'string',
      hostname: 'string',
      numberOfInstances: 7,
      id: 8,
      ea: false,
      watchdogUpdatedOnLocal: 'string',
      hasFailOverDevice: false,
      collectorGroupName: 'string',
      netscanVersion: 'string',
      inSDT: true,
      updatedOn: 5,
      automaticUpgradeInfo: null,
      numberOfHosts: 8,
      collectorConf: 'string',
      sbproxyConf: 'string',
      userVisibleWebsitesNum: 7,
      lastSentNotificationOnLocal: 'string',
      customProperties: [
        {
          name: 'addr',
          value: '127.0.0.1'
        }
      ],
      predefinedConfig: {},
      onetimeUpgradeInfo: null,
      wrapperConf: 'string',
      clearSent: false,
      status: 10,
      backupAgentId: 75,
      specifiedCollectorDeviceGroupId: 3,
      escalatingChainId: 80,
      collectorSize: 'string',
      ackedOnLocal: 'string',
      watchdogConf: 'string',
      description: 'Linux Collector',
      createdOn: 5,
      platform: 'string',
      isDown: false,
      userVisibleHostsNum: 6,
      canDowngrade: false,
      ackComment: 'string',
      nextUpgradeInfo: null,
      suppressAlertClear: true,
      nextRecipient: 9,
      ackedOn: 3,
      userChangeOnLocal: 'string',
      numberOfWebsites: 8,
      collectorDeviceId: 4,
      ackedBy: 'string',
      userPermission: 'string',
      needAutoCreateCollectorDevice: 'true',
      watchdogUpdatedOn: 3,
      canDowngradeReason: 'string',
      lastSentNotificationOn: 7,
      acked: false,
      onetimeDowngradeInfo: null,
      websiteConf: 'string',
      upTime: 3,
      build: 'string',
      previousVersion: 'string',
      collectorGroupId: 10,
      createdOnLocal: 'string',
      enableFailBack: 'true',
      resendIval: 15,
      arch: 'string',
      enableFailOverOnCollectorDevice: true
    };
    describe('#updateCollectorById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCollectorById(collectorsId, collectorsUpdateCollectorByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collectors', 'updateCollectorById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectorsPatchCollectorByIdBodyParam = {
      userChangeOn: 6,
      confVersion: 'string',
      updatedOnLocal: 'string',
      hostname: 'string',
      numberOfInstances: 2,
      id: 9,
      ea: true,
      watchdogUpdatedOnLocal: 'string',
      hasFailOverDevice: false,
      collectorGroupName: 'string',
      netscanVersion: 'string',
      inSDT: false,
      updatedOn: 2,
      automaticUpgradeInfo: null,
      numberOfHosts: 2,
      collectorConf: 'string',
      sbproxyConf: 'string',
      userVisibleWebsitesNum: 5,
      lastSentNotificationOnLocal: 'string',
      customProperties: [
        {
          name: 'addr',
          value: '127.0.0.1'
        }
      ],
      predefinedConfig: {},
      onetimeUpgradeInfo: null,
      wrapperConf: 'string',
      clearSent: false,
      status: 4,
      backupAgentId: 75,
      specifiedCollectorDeviceGroupId: 10,
      escalatingChainId: 80,
      collectorSize: 'string',
      ackedOnLocal: 'string',
      watchdogConf: 'string',
      description: 'Linux Collector',
      createdOn: 1,
      platform: 'string',
      isDown: false,
      userVisibleHostsNum: 4,
      canDowngrade: false,
      ackComment: 'string',
      nextUpgradeInfo: null,
      suppressAlertClear: true,
      nextRecipient: 7,
      ackedOn: 4,
      userChangeOnLocal: 'string',
      numberOfWebsites: 8,
      collectorDeviceId: 7,
      ackedBy: 'string',
      userPermission: 'string',
      needAutoCreateCollectorDevice: 'true',
      watchdogUpdatedOn: 6,
      canDowngradeReason: 'string',
      lastSentNotificationOn: 3,
      acked: false,
      onetimeDowngradeInfo: null,
      websiteConf: 'string',
      upTime: 8,
      build: 'string',
      previousVersion: 'string',
      collectorGroupId: 10,
      createdOnLocal: 'string',
      enableFailBack: 'true',
      resendIval: 15,
      arch: 'string',
      enableFailOverOnCollectorDevice: true
    };
    describe('#patchCollectorById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchCollectorById(collectorsId, collectorsPatchCollectorByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collectors', 'patchCollectorById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectorById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCollectorById(collectorsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.userChangeOn);
                assert.equal('string', data.response.confVersion);
                assert.equal('string', data.response.updatedOnLocal);
                assert.equal('string', data.response.hostname);
                assert.equal(3, data.response.numberOfInstances);
                assert.equal(7, data.response.id);
                assert.equal(true, data.response.ea);
                assert.equal('string', data.response.watchdogUpdatedOnLocal);
                assert.equal(false, data.response.hasFailOverDevice);
                assert.equal('string', data.response.collectorGroupName);
                assert.equal('string', data.response.netscanVersion);
                assert.equal(false, data.response.inSDT);
                assert.equal(3, data.response.updatedOn);
                assert.equal(5, data.response.numberOfHosts);
                assert.equal('string', data.response.collectorConf);
                assert.equal('string', data.response.sbproxyConf);
                assert.equal(3, data.response.userVisibleWebsitesNum);
                assert.equal('string', data.response.lastSentNotificationOnLocal);
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal('object', typeof data.response.predefinedConfig);
                assert.equal('string', data.response.wrapperConf);
                assert.equal(true, data.response.clearSent);
                assert.equal(3, data.response.status);
                assert.equal(75, data.response.backupAgentId);
                assert.equal(2, data.response.specifiedCollectorDeviceGroupId);
                assert.equal(80, data.response.escalatingChainId);
                assert.equal('string', data.response.collectorSize);
                assert.equal('string', data.response.ackedOnLocal);
                assert.equal('string', data.response.watchdogConf);
                assert.equal('Linux Collector', data.response.description);
                assert.equal(9, data.response.createdOn);
                assert.equal('string', data.response.platform);
                assert.equal(false, data.response.isDown);
                assert.equal(9, data.response.userVisibleHostsNum);
                assert.equal(false, data.response.canDowngrade);
                assert.equal('string', data.response.ackComment);
                assert.equal(true, data.response.suppressAlertClear);
                assert.equal(10, data.response.nextRecipient);
                assert.equal(9, data.response.ackedOn);
                assert.equal('string', data.response.userChangeOnLocal);
                assert.equal(2, data.response.numberOfWebsites);
                assert.equal(4, data.response.collectorDeviceId);
                assert.equal('string', data.response.ackedBy);
                assert.equal('string', data.response.userPermission);
                assert.equal('true', data.response.needAutoCreateCollectorDevice);
                assert.equal(9, data.response.watchdogUpdatedOn);
                assert.equal('string', data.response.canDowngradeReason);
                assert.equal(1, data.response.lastSentNotificationOn);
                assert.equal(false, data.response.acked);
                assert.equal('string', data.response.websiteConf);
                assert.equal(1, data.response.upTime);
                assert.equal('string', data.response.build);
                assert.equal('string', data.response.previousVersion);
                assert.equal(10, data.response.collectorGroupId);
                assert.equal('string', data.response.createdOnLocal);
                assert.equal('true', data.response.enableFailBack);
                assert.equal(15, data.response.resendIval);
                assert.equal('string', data.response.arch);
                assert.equal(true, data.response.enableFailOverOnCollectorDevice);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collectors', 'getCollectorById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let thresholdsId = 'fakedata';
    const thresholdsDeviceGroupId = 555;
    const thresholdsAddDeviceGroupClusterAlertConfBodyParam = {
      countBy: 'host',
      dataPointName: 'InMbps',
      dataSourceId: 396,
      dataPointId: 2016,
      minAlertLevel: 4,
      dataSourceDisplayName: 'Interfaces (64 bit)-',
      disableAlerting: true,
      id: 3,
      suppressIndAlert: 'true',
      thresholdType: 'absolute',
      dataPointDescription: 'Inbound throughput, in megabits per second.\n\nBest choice for reports, etc.',
      alertExpr: '>=2'
    };
    describe('#addDeviceGroupClusterAlertConf - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDeviceGroupClusterAlertConf(thresholdsDeviceGroupId, thresholdsAddDeviceGroupClusterAlertConfBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('host', data.response.countBy);
                assert.equal('InMbps', data.response.dataPointName);
                assert.equal(396, data.response.dataSourceId);
                assert.equal(2016, data.response.dataPointId);
                assert.equal(4, data.response.minAlertLevel);
                assert.equal('Interfaces (64 bit)-', data.response.dataSourceDisplayName);
                assert.equal(true, data.response.disableAlerting);
                assert.equal(2, data.response.id);
                assert.equal('true', data.response.suppressIndAlert);
                assert.equal('absolute', data.response.thresholdType);
                assert.equal('Inbound throughput, in megabits per second.\n\nBest choice for reports, etc.', data.response.dataPointDescription);
                assert.equal('>=2', data.response.alertExpr);
              } else {
                runCommonAsserts(data, error);
              }
              thresholdsId = data.response.id;
              saveMockData('Thresholds', 'addDeviceGroupClusterAlertConf', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsDeviceId = 555;
    const thresholdsHdsId = 555;
    const thresholdsInstanceId = 555;
    describe('#getDeviceDatasourceInstanceAlertSettingListOfDSI - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceAlertSettingListOfDSI(thresholdsDeviceId, thresholdsHdsId, thresholdsInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getDeviceDatasourceInstanceAlertSettingListOfDSI', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsUpdateDeviceDatasourceInstanceAlertSettingByIdBodyParam = {
      globalAlertExpr: 'string',
      alertClearInterval: 1,
      disableAlerting: true,
      alertExprNote: 'Router 1 Ping Check',
      dataPointDescription: 'string',
      dataSourceInstanceId: 6,
      disableDpAlertHostGroups: 'string',
      dataPointName: 'string',
      dataPointId: 4,
      deviceGroupId: 10,
      parentDeviceGroupAlertExprList: [
        {
          userPermission: 'string',
          groupId: 1,
          alertEnabled: false,
          groupFullPath: 'string',
          alertExpr: 'string'
        }
      ],
      alertingDisabledOn: 'string',
      id: 5,
      dataSourceInstanceAlias: 'string',
      deviceGroupFullPath: 'string',
      alertExpr: '>= 70 90 95',
      alertTransitionInterval: 3
    };
    describe('#updateDeviceDatasourceInstanceAlertSettingById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceDatasourceInstanceAlertSettingById(thresholdsDeviceId, thresholdsHdsId, thresholdsInstanceId, thresholdsId, thresholdsUpdateDeviceDatasourceInstanceAlertSettingByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'updateDeviceDatasourceInstanceAlertSettingById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsPatchDeviceDatasourceInstanceAlertSettingByIdBodyParam = {
      globalAlertExpr: 'string',
      alertClearInterval: 7,
      disableAlerting: true,
      alertExprNote: 'Router 1 Ping Check',
      dataPointDescription: 'string',
      dataSourceInstanceId: 6,
      disableDpAlertHostGroups: 'string',
      dataPointName: 'string',
      dataPointId: 4,
      deviceGroupId: 6,
      parentDeviceGroupAlertExprList: [
        {
          userPermission: 'string',
          groupId: 1,
          alertEnabled: false,
          groupFullPath: 'string',
          alertExpr: 'string'
        }
      ],
      alertingDisabledOn: 'string',
      id: 6,
      dataSourceInstanceAlias: 'string',
      deviceGroupFullPath: 'string',
      alertExpr: '>= 70 90 95',
      alertTransitionInterval: 4
    };
    describe('#patchDeviceDatasourceInstanceAlertSettingById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDeviceDatasourceInstanceAlertSettingById(thresholdsDeviceId, thresholdsHdsId, thresholdsInstanceId, thresholdsId, thresholdsPatchDeviceDatasourceInstanceAlertSettingByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'patchDeviceDatasourceInstanceAlertSettingById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDatasourceInstanceAlertSettingById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDatasourceInstanceAlertSettingById(thresholdsDeviceId, thresholdsHdsId, thresholdsInstanceId, thresholdsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.globalAlertExpr);
                assert.equal(9, data.response.alertClearInterval);
                assert.equal(true, data.response.disableAlerting);
                assert.equal('Router 1 Ping Check', data.response.alertExprNote);
                assert.equal('string', data.response.dataPointDescription);
                assert.equal(6, data.response.dataSourceInstanceId);
                assert.equal('string', data.response.disableDpAlertHostGroups);
                assert.equal('string', data.response.dataPointName);
                assert.equal(4, data.response.dataPointId);
                assert.equal(10, data.response.deviceGroupId);
                assert.equal(true, Array.isArray(data.response.parentDeviceGroupAlertExprList));
                assert.equal('string', data.response.alertingDisabledOn);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.dataSourceInstanceAlias);
                assert.equal('string', data.response.deviceGroupFullPath);
                assert.equal('>= 70 90 95', data.response.alertExpr);
                assert.equal(5, data.response.alertTransitionInterval);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getDeviceDatasourceInstanceAlertSettingById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupClusterAlertConfList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupClusterAlertConfList(thresholdsDeviceGroupId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getDeviceGroupClusterAlertConfList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsUpdateDeviceGroupClusterAlertConfByIdBodyParam = {
      countBy: 'host',
      dataPointName: 'InMbps',
      dataSourceId: 396,
      dataPointId: 2016,
      minAlertLevel: 4,
      dataSourceDisplayName: 'Interfaces (64 bit)-',
      disableAlerting: false,
      id: 8,
      suppressIndAlert: 'true',
      thresholdType: 'absolute',
      dataPointDescription: 'Inbound throughput, in megabits per second.\n\nBest choice for reports, etc.',
      alertExpr: '>=2'
    };
    describe('#updateDeviceGroupClusterAlertConfById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupClusterAlertConfById(thresholdsDeviceGroupId, thresholdsId, thresholdsUpdateDeviceGroupClusterAlertConfByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'updateDeviceGroupClusterAlertConfById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsPatchDeviceGroupClusterAlertConfByIdBodyParam = {
      countBy: 'host',
      dataPointName: 'InMbps',
      dataSourceId: 396,
      dataPointId: 2016,
      minAlertLevel: 4,
      dataSourceDisplayName: 'Interfaces (64 bit)-',
      disableAlerting: false,
      id: 10,
      suppressIndAlert: 'true',
      thresholdType: 'absolute',
      dataPointDescription: 'Inbound throughput, in megabits per second.\n\nBest choice for reports, etc.',
      alertExpr: '>=2'
    };
    describe('#patchDeviceGroupClusterAlertConfById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDeviceGroupClusterAlertConfById(thresholdsDeviceGroupId, thresholdsId, thresholdsPatchDeviceGroupClusterAlertConfByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'patchDeviceGroupClusterAlertConfById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupClusterAlertConfById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupClusterAlertConfById(thresholdsDeviceGroupId, thresholdsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('host', data.response.countBy);
                assert.equal('InMbps', data.response.dataPointName);
                assert.equal(396, data.response.dataSourceId);
                assert.equal(2016, data.response.dataPointId);
                assert.equal(4, data.response.minAlertLevel);
                assert.equal('Interfaces (64 bit)-', data.response.dataSourceDisplayName);
                assert.equal(false, data.response.disableAlerting);
                assert.equal(10, data.response.id);
                assert.equal('true', data.response.suppressIndAlert);
                assert.equal('absolute', data.response.thresholdType);
                assert.equal('Inbound throughput, in megabits per second.\n\nBest choice for reports, etc.', data.response.dataPointDescription);
                assert.equal('>=2', data.response.alertExpr);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getDeviceGroupClusterAlertConfById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsDsId = 555;
    const thresholdsUpdateDeviceGroupDatasourceAlertSettingBodyParam = {
      datasourceType: 'string',
      dpConfig: [
        {
          globalAlertExpr: 'string',
          dataPointName: 'string',
          dataPointId: 3,
          disableAlerting: false,
          alertExprNote: 'string',
          alertExpr: 'string',
          dataPointDescription: 'string'
        }
      ]
    };
    describe('#updateDeviceGroupDatasourceAlertSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupDatasourceAlertSetting(thresholdsDeviceGroupId, thresholdsDsId, thresholdsUpdateDeviceGroupDatasourceAlertSettingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'updateDeviceGroupDatasourceAlertSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsPatchDeviceGroupDatasourceAlertSettingBodyParam = {
      datasourceType: 'string',
      dpConfig: [
        {
          globalAlertExpr: 'string',
          dataPointName: 'string',
          dataPointId: 8,
          disableAlerting: true,
          alertExprNote: 'string',
          alertExpr: 'string',
          dataPointDescription: 'string'
        }
      ]
    };
    describe('#patchDeviceGroupDatasourceAlertSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDeviceGroupDatasourceAlertSetting(thresholdsDeviceGroupId, thresholdsDsId, thresholdsPatchDeviceGroupDatasourceAlertSettingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'patchDeviceGroupDatasourceAlertSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupDatasourceAlertSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupDatasourceAlertSetting(thresholdsDeviceGroupId, thresholdsDsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.datasourceType);
                assert.equal(true, Array.isArray(data.response.dpConfig));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getDeviceGroupDatasourceAlertSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let recipientGroupsId = 'fakedata';
    const recipientGroupsAddRecipientGroupBodyParam = {
      groupName: 'Tier 1 Helpdesk'
    };
    describe('#addRecipientGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRecipientGroup(recipientGroupsAddRecipientGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Tier 1 Helpdesk', data.response.groupName);
                assert.equal(true, Array.isArray(data.response.recipients));
                assert.equal('Tier 1 Helpdesk', data.response.description);
                assert.equal(8, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              recipientGroupsId = data.response.id;
              saveMockData('RecipientGroups', 'addRecipientGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecipientGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRecipientGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RecipientGroups', 'getRecipientGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recipientGroupsUpdateRecipientGroupByIdBodyParam = {
      groupName: 'Tier 1 Helpdesk'
    };
    describe('#updateRecipientGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRecipientGroupById(recipientGroupsId, recipientGroupsUpdateRecipientGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RecipientGroups', 'updateRecipientGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recipientGroupsPatchRecipientGroupByIdBodyParam = {
      groupName: 'Tier 1 Helpdesk'
    };
    describe('#patchRecipientGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchRecipientGroupById(recipientGroupsId, recipientGroupsPatchRecipientGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RecipientGroups', 'patchRecipientGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecipientGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRecipientGroupById(recipientGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Tier 1 Helpdesk', data.response.groupName);
                assert.equal(true, Array.isArray(data.response.recipients));
                assert.equal('Tier 1 Helpdesk', data.response.description);
                assert.equal(7, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RecipientGroups', 'getRecipientGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let reportGroupsId = 'fakedata';
    const reportGroupsAddReportGroupBodyParam = {
      matchedReportCount: 5,
      userPermission: 'string',
      name: 'string',
      description: 'string',
      id: 6,
      reportsCount: 3
    };
    describe('#addReportGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addReportGroup(reportGroupsAddReportGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.matchedReportCount);
                assert.equal('string', data.response.userPermission);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal(5, data.response.reportsCount);
              } else {
                runCommonAsserts(data, error);
              }
              reportGroupsId = data.response.id;
              saveMockData('ReportGroups', 'addReportGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportGroups', 'getReportGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportGroupsUpdateReportGroupByIdBodyParam = {
      matchedReportCount: 8,
      userPermission: 'string',
      name: 'string',
      description: 'string',
      id: 5,
      reportsCount: 5
    };
    describe('#updateReportGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReportGroupById(reportGroupsId, reportGroupsUpdateReportGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportGroups', 'updateReportGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportGroupsPatchReportGroupByIdBodyParam = {
      matchedReportCount: 9,
      userPermission: 'string',
      name: 'string',
      description: 'string',
      id: 6,
      reportsCount: 2
    };
    describe('#patchReportGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchReportGroupById(reportGroupsId, reportGroupsPatchReportGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportGroups', 'patchReportGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportGroupById(reportGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.matchedReportCount);
                assert.equal('string', data.response.userPermission);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.id);
                assert.equal(1, data.response.reportsCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportGroups', 'getReportGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let rolesId = 'fakedata';
    const rolesAddRoleBodyParam = {
      privileges: [
        {}
      ],
      name: 'administrator'
    };
    describe('#addRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRole(rolesAddRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.enableRemoteSessionInCompanyLevel);
                assert.equal('Internal Support Resources', data.response.customHelpLabel);
                assert.equal('https://logicmonitor.com/support', data.response.customHelpURL);
                assert.equal(true, Array.isArray(data.response.privileges));
                assert.equal(8, data.response.associatedUserCount);
                assert.equal('administrator', data.response.name);
                assert.equal('Administrator can do everything, including security-sensitive actions', data.response.description);
                assert.equal(9, data.response.id);
                assert.equal(true, data.response.twoFARequired);
                assert.equal(true, data.response.requireEULA);
                assert.equal(false, data.response.acctRequireTwoFA);
              } else {
                runCommonAsserts(data, error);
              }
              rolesId = data.response.id;
              saveMockData('Roles', 'addRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoleList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'getRoleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesUpdateRoleByIdBodyParam = {
      privileges: [
        {}
      ],
      name: 'administrator'
    };
    describe('#updateRoleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRoleById(rolesId, rolesUpdateRoleByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'updateRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesPatchRoleByIdBodyParam = {
      privileges: [
        {}
      ],
      name: 'administrator'
    };
    describe('#patchRoleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchRoleById(rolesId, rolesPatchRoleByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'patchRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoleById(rolesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enableRemoteSessionInCompanyLevel);
                assert.equal('Internal Support Resources', data.response.customHelpLabel);
                assert.equal('https://logicmonitor.com/support', data.response.customHelpURL);
                assert.equal(true, Array.isArray(data.response.privileges));
                assert.equal(8, data.response.associatedUserCount);
                assert.equal('administrator', data.response.name);
                assert.equal('Administrator can do everything, including security-sensitive actions', data.response.description);
                assert.equal(2, data.response.id);
                assert.equal(true, data.response.twoFARequired);
                assert.equal(true, data.response.requireEULA);
                assert.equal(true, data.response.acctRequireTwoFA);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'getRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let netscansId = 'fakedata';
    const netscansAddNetscanBodyParam = {
      method: 'nmap',
      duplicate: null,
      collector: 148,
      name: 'Office Network'
    };
    describe('#addNetscan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNetscan(netscansAddNetscanBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('sarah@logicmonitor.com', data.response.creator);
                assert.equal('Prod', data.response.collectorGroupName);
                assert.equal('nmap', data.response.method);
                assert.equal(4, data.response.collectorGroup);
                assert.equal('Discovers devices within the office network for monitoring', data.response.description);
                assert.equal('2018-09-07 15:12:00 PDT', data.response.nextStart);
                assert.equal(2, data.response.version);
                assert.equal(148, data.response.collector);
                assert.equal(false, data.response.ignoreSystemIPsDuplicates);
                assert.equal('Prod CollectorA', data.response.collectorDescription);
                assert.equal('Office Network', data.response.name);
                assert.equal(1536358320, data.response.nextStartEpoch);
                assert.equal(208, data.response.id);
                assert.equal(47, data.response.nsgId);
                assert.equal('RT_check', data.response.group);
              } else {
                runCommonAsserts(data, error);
              }
              netscansId = data.response.id;
              saveMockData('Netscans', 'addNetscan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetscanList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetscanList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netscans', 'getNetscanList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netscansUpdateNetscanBodyParam = {
      method: 'nmap',
      duplicate: null,
      collector: 148,
      name: 'Office Network'
    };
    describe('#updateNetscan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetscan(netscansUpdateNetscanBodyParam, null, netscansId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netscans', 'updateNetscan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netscansPatchNetscanBodyParam = {
      method: 'nmap',
      duplicate: null,
      collector: 148,
      name: 'Office Network'
    };
    describe('#patchNetscan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchNetscan(netscansPatchNetscanBodyParam, null, netscansId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netscans', 'patchNetscan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetscanById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetscanById(netscansId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('sarah@logicmonitor.com', data.response.creator);
                assert.equal('Prod', data.response.collectorGroupName);
                assert.equal('nmap', data.response.method);
                assert.equal(4, data.response.collectorGroup);
                assert.equal('Discovers devices within the office network for monitoring', data.response.description);
                assert.equal('2018-09-07 15:12:00 PDT', data.response.nextStart);
                assert.equal(2, data.response.version);
                assert.equal(148, data.response.collector);
                assert.equal(true, data.response.ignoreSystemIPsDuplicates);
                assert.equal('Prod CollectorA', data.response.collectorDescription);
                assert.equal('Office Network', data.response.name);
                assert.equal(1536358320, data.response.nextStartEpoch);
                assert.equal(208, data.response.id);
                assert.equal(47, data.response.nsgId);
                assert.equal('RT_check', data.response.group);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netscans', 'getNetscanById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let websiteGroupsId = 'fakedata';
    const websiteGroupsAddWebsiteGroupBodyParam = {
      name: 'Amazon Website Checks'
    };
    describe('#addWebsiteGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addWebsiteGroup(websiteGroupsAddWebsiteGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fullPath);
                assert.equal(true, data.response.stopMonitoring);
                assert.equal('string', data.response.userPermission);
                assert.equal('object', typeof data.response.testLocation);
                assert.equal(false, data.response.hasWebsitesDisabled);
                assert.equal('Amazon web and ping checks', data.response.description);
                assert.equal(true, data.response.disableAlerting);
                assert.equal(1, data.response.parentId);
                assert.equal(10, data.response.numOfDirectWebsites);
                assert.equal('Amazon Website Checks', data.response.name);
                assert.equal(6, data.response.numOfDirectSubGroups);
                assert.equal(4, data.response.id);
                assert.equal(2, data.response.numOfWebsites);
                assert.equal(true, Array.isArray(data.response.properties));
              } else {
                runCommonAsserts(data, error);
              }
              websiteGroupsId = data.response.id;
              saveMockData('WebsiteGroups', 'addWebsiteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebsiteGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebsiteGroups', 'getWebsiteGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const websiteGroupsUpdateWebsiteGroupByIdBodyParam = {
      name: 'Amazon Website Checks'
    };
    describe('#updateWebsiteGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWebsiteGroupById(websiteGroupsId, websiteGroupsUpdateWebsiteGroupByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebsiteGroups', 'updateWebsiteGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const websiteGroupsPatchWebsiteGroupByIdBodyParam = {
      name: 'Amazon Website Checks'
    };
    describe('#patchWebsiteGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchWebsiteGroupById(websiteGroupsId, websiteGroupsPatchWebsiteGroupByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebsiteGroups', 'patchWebsiteGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebsiteGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebsiteGroupById(websiteGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fullPath);
                assert.equal(true, data.response.stopMonitoring);
                assert.equal('string', data.response.userPermission);
                assert.equal('object', typeof data.response.testLocation);
                assert.equal(false, data.response.hasWebsitesDisabled);
                assert.equal('Amazon web and ping checks', data.response.description);
                assert.equal(true, data.response.disableAlerting);
                assert.equal(1, data.response.parentId);
                assert.equal(1, data.response.numOfDirectWebsites);
                assert.equal('Amazon Website Checks', data.response.name);
                assert.equal(4, data.response.numOfDirectSubGroups);
                assert.equal(10, data.response.id);
                assert.equal(2, data.response.numOfWebsites);
                assert.equal(true, Array.isArray(data.response.properties));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebsiteGroups', 'getWebsiteGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDTHistoryByWebsiteGroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDTHistoryByWebsiteGroupId(websiteGroupsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebsiteGroups', 'getSDTHistoryByWebsiteGroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSDTListByWebsiteGroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSDTListByWebsiteGroupId(websiteGroupsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebsiteGroups', 'getAllSDTListByWebsiteGroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImmediateWebsiteListByWebsiteGroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImmediateWebsiteListByWebsiteGroupId(websiteGroupsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebsiteGroups', 'getImmediateWebsiteListByWebsiteGroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debugExecuteDebugCommandBodyParam = {
      output: 'string',
      cmdline: 'string',
      cmdContext: 'string',
      sessionId: 'string'
    };
    describe('#executeDebugCommand - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeDebugCommand(debugExecuteDebugCommandBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.output);
                assert.equal('string', data.response.cmdline);
                assert.equal('string', data.response.cmdContext);
                assert.equal('string', data.response.sessionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Debug', 'executeDebugCommand', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debugId = 'fakedata';
    describe('#getDebugCommandResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDebugCommandResult(debugId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.output);
                assert.equal('string', data.response.cmdline);
                assert.equal('string', data.response.cmdContext);
                assert.equal('string', data.response.sessionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Debug', 'getDebugCommandResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const batchJobsFile = 'fakedata';
    describe('#importBatchJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importBatchJob(batchJobsFile, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BatchJobs', 'importBatchJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let widgetsId = 'fakedata';
    const widgetsAddWidgetBodyParam = {
      dashboardId: 1,
      name: 'Test',
      type: 'bigNumber'
    };
    describe('#addWidget - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addWidget(widgetsAddWidgetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('write', data.response.userPermission);
                assert.equal(1, data.response.dashboardId);
                assert.equal('Test', data.response.name);
                assert.equal('Devices By Type', data.response.description);
                assert.equal(2, data.response.lastUpdatedOn);
                assert.equal('newBorderBlue', data.response.theme);
                assert.equal(5, data.response.interval);
                assert.equal(8, data.response.id);
                assert.equal('bigNumber', data.response.type);
                assert.equal('string', data.response.timescale);
              } else {
                runCommonAsserts(data, error);
              }
              widgetsId = data.response.id;
              saveMockData('Widgets', 'addWidget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWidgetList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWidgetList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Widgets', 'getWidgetList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const widgetsUpdateWidgetByIdBodyParam = {
      dashboardId: 1,
      name: 'Test',
      type: 'bigNumber'
    };
    describe('#updateWidgetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWidgetById(widgetsId, widgetsUpdateWidgetByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Widgets', 'updateWidgetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const widgetsPatchWidgetByIdBodyParam = {
      dashboardId: 1,
      name: 'Test',
      type: 'bigNumber'
    };
    describe('#patchWidgetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchWidgetById(widgetsId, widgetsPatchWidgetByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Widgets', 'patchWidgetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWidgetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWidgetById(widgetsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('write', data.response.userPermission);
                assert.equal(1, data.response.dashboardId);
                assert.equal('Test', data.response.name);
                assert.equal('Devices By Type', data.response.description);
                assert.equal(4, data.response.lastUpdatedOn);
                assert.equal('newBorderBlue', data.response.theme);
                assert.equal(5, data.response.interval);
                assert.equal(10, data.response.id);
                assert.equal('bigNumber', data.response.type);
                assert.equal('string', data.response.timescale);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Widgets', 'getWidgetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let aPITokensAdminId = 555;
    let aPITokensApitokenId = 555;
    const aPITokensAddApiTokenByAdminIdBodyParam = {
      accessId: 'string',
      adminName: 'string',
      note: 'John Doe\'s API Token',
      lastUsedOn: 4,
      accessKey: 'string',
      createdBy: 'string',
      roles: [
        'string'
      ],
      adminId: 1,
      id: 5,
      createdOn: 5,
      status: 2
    };
    describe('#addApiTokenByAdminId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addApiTokenByAdminId(aPITokensAdminId, aPITokensAddApiTokenByAdminIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.accessId);
                assert.equal('string', data.response.adminName);
                assert.equal('John Doe\'s API Token', data.response.note);
                assert.equal(4, data.response.lastUsedOn);
                assert.equal('string', data.response.accessKey);
                assert.equal('string', data.response.createdBy);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(7, data.response.adminId);
                assert.equal(3, data.response.id);
                assert.equal(7, data.response.createdOn);
                assert.equal(2, data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              aPITokensAdminId = data.response.id;
              aPITokensApitokenId = data.response.id;
              saveMockData('APITokens', 'addApiTokenByAdminId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiTokenList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiTokenList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('APITokens', 'getApiTokenList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiTokenListByAdminId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiTokenListByAdminId(aPITokensAdminId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('APITokens', 'getApiTokenListByAdminId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aPITokensUpdateApiTokenByAdminIdBodyParam = {
      accessId: 'string',
      adminName: 'string',
      note: 'John Doe\'s API Token',
      lastUsedOn: 5,
      accessKey: 'string',
      createdBy: 'string',
      roles: [
        'string'
      ],
      adminId: 3,
      id: 9,
      createdOn: 1,
      status: 2
    };
    describe('#updateApiTokenByAdminId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApiTokenByAdminId(aPITokensAdminId, aPITokensApitokenId, aPITokensUpdateApiTokenByAdminIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('APITokens', 'updateApiTokenByAdminId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aPITokensPatchApiTokenByAdminIdBodyParam = {
      accessId: 'string',
      adminName: 'string',
      note: 'John Doe\'s API Token',
      lastUsedOn: 3,
      accessKey: 'string',
      createdBy: 'string',
      roles: [
        'string'
      ],
      adminId: 1,
      id: 1,
      createdOn: 4,
      status: 2
    };
    describe('#patchApiTokenByAdminId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchApiTokenByAdminId(aPITokensAdminId, aPITokensApitokenId, aPITokensPatchApiTokenByAdminIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('APITokens', 'patchApiTokenByAdminId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalApiStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExternalApiStats((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('get alert list', data.response.summary);
                assert.equal(10, data.response.totalWaitingRequests);
                assert.equal(10, data.response.totNanoTime);
                assert.equal(10, data.response.totalProcessedRequests);
                assert.equal(10, data.response.totalRequests);
                assert.equal('10', data.response.api);
                assert.equal(10, data.response.maxNanoTime);
                assert.equal('Devices, Device Groups', data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiPerfStats', 'getExternalApiStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sDTsId = 'fakedata';
    const sDTsAddSDTBodyParam = {
      type: 'DeviceGroupSDT'
    };
    describe('#addSDT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSDT(sDTsAddSDTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.endDateTimeOnLocal);
                assert.equal('America/Los_Angeles', data.response.timezone);
                assert.equal('oneTime', data.response.sdtType);
                assert.equal(7, data.response.monthDay);
                assert.equal('1', data.response.weekOfMonth);
                assert.equal('string', data.response.admin);
                assert.equal(1534554000000, data.response.endDateTime);
                assert.equal('DeviceGroupSDT', data.response.type);
                assert.equal(true, data.response.isEffective);
                assert.equal(6, data.response.minute);
                assert.equal(138, data.response.duration);
                assert.equal(5, data.response.endHour);
                assert.equal(1534460400000, data.response.startDateTime);
                assert.equal(3, data.response.hour);
                assert.equal('string', data.response.startDateTimeOnLocal);
                assert.equal('Sunday', data.response.weekDay);
                assert.equal('Emergency prod deployment', data.response.comment);
                assert.equal('string', data.response.id);
                assert.equal(18, data.response.endMinute);
              } else {
                runCommonAsserts(data, error);
              }
              sDTsId = data.response.id;
              saveMockData('SDTs', 'addSDT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDTList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDTList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDTs', 'getSDTList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDTsUpdateSDTByIdBodyParam = {
      type: 'DeviceGroupSDT'
    };
    describe('#updateSDTById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSDTById(sDTsId, sDTsUpdateSDTByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDTs', 'updateSDTById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sDTsPatchSDTByIdBodyParam = {
      type: 'DeviceGroupSDT'
    };
    describe('#patchSDTById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchSDTById(sDTsId, sDTsPatchSDTByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDTs', 'patchSDTById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDTById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDTById(sDTsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.endDateTimeOnLocal);
                assert.equal('America/Los_Angeles', data.response.timezone);
                assert.equal('oneTime', data.response.sdtType);
                assert.equal(7, data.response.monthDay);
                assert.equal('1', data.response.weekOfMonth);
                assert.equal('string', data.response.admin);
                assert.equal(1534554000000, data.response.endDateTime);
                assert.equal('DeviceGroupSDT', data.response.type);
                assert.equal(false, data.response.isEffective);
                assert.equal(6, data.response.minute);
                assert.equal(138, data.response.duration);
                assert.equal(5, data.response.endHour);
                assert.equal(1534460400000, data.response.startDateTime);
                assert.equal(3, data.response.hour);
                assert.equal('string', data.response.startDateTimeOnLocal);
                assert.equal('Sunday', data.response.weekDay);
                assert.equal('Emergency prod deployment', data.response.comment);
                assert.equal('string', data.response.id);
                assert.equal(18, data.response.endMinute);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDTs', 'getSDTById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSMappingsFile = 'fakedata';
    describe('#importDNSMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importDNSMapping(dNSMappingsFile, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSMappings', 'importDNSMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dashboardGroupsId = 'fakedata';
    const dashboardGroupsAddDashboardGroupBodyParam = {
      name: 'LogicMonitor Dashboards'
    };
    describe('#addDashboardGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDashboardGroup(dashboardGroupsAddDashboardGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.template);
                assert.equal('string', data.response.fullPath);
                assert.equal('string', data.response.userPermission);
                assert.equal(true, Array.isArray(data.response.widgetTokens));
                assert.equal('LogicMonitor Dashboards', data.response.name);
                assert.equal(10, data.response.numOfDirectSubGroups);
                assert.equal(6, data.response.numOfDashboards);
                assert.equal('Servers in LA DataCenter', data.response.description);
                assert.equal(10, data.response.id);
                assert.equal(true, Array.isArray(data.response.dashboards));
                assert.equal(1, data.response.parentId);
                assert.equal(3, data.response.numOfDirectDashboards);
              } else {
                runCommonAsserts(data, error);
              }
              dashboardGroupsId = data.response.id;
              saveMockData('DashboardGroups', 'addDashboardGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDashboardGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDashboardGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardGroups', 'getDashboardGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardGroupsUpdateDashboardGroupByIdBodyParam = {
      name: 'LogicMonitor Dashboards'
    };
    describe('#updateDashboardGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDashboardGroupById(dashboardGroupsId, dashboardGroupsUpdateDashboardGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardGroups', 'updateDashboardGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardGroupsPatchDashboardGroupByIdBodyParam = {
      name: 'LogicMonitor Dashboards'
    };
    describe('#patchDashboardGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDashboardGroupById(dashboardGroupsId, dashboardGroupsPatchDashboardGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardGroups', 'patchDashboardGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDashboardGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDashboardGroupById(dashboardGroupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.template);
                assert.equal('string', data.response.fullPath);
                assert.equal('string', data.response.userPermission);
                assert.equal(true, Array.isArray(data.response.widgetTokens));
                assert.equal('LogicMonitor Dashboards', data.response.name);
                assert.equal(4, data.response.numOfDirectSubGroups);
                assert.equal(3, data.response.numOfDashboards);
                assert.equal('Servers in LA DataCenter', data.response.description);
                assert.equal(8, data.response.id);
                assert.equal(true, Array.isArray(data.response.dashboards));
                assert.equal(1, data.response.parentId);
                assert.equal(3, data.response.numOfDirectDashboards);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardGroups', 'getDashboardGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let collectorGroupsId = 'fakedata';
    const collectorGroupsAddCollectorGroupBodyParam = {
      name: 'Collector (Network Devices)'
    };
    describe('#addCollectorGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCollectorGroup(collectorGroupsAddCollectorGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.autoBalance);
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal('string', data.response.userPermission);
                assert.equal(6, data.response.numOfCollectors);
                assert.equal('Collector (Network Devices)', data.response.name);
                assert.equal(6, data.response.autoBalanceInstanceCountThreshold);
                assert.equal('string', data.response.autoBalanceStrategy);
                assert.equal('Group for collectors dedicated to Network Devices', data.response.description);
                assert.equal(2, data.response.createOn);
                assert.equal(5, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              collectorGroupsId = data.response.id;
              saveMockData('CollectorGroups', 'addCollectorGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectorGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCollectorGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CollectorGroups', 'getCollectorGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectorGroupsUpdateCollectorGroupByIdBodyParam = {
      name: 'Collector (Network Devices)'
    };
    describe('#updateCollectorGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCollectorGroupById(collectorGroupsId, collectorGroupsUpdateCollectorGroupByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CollectorGroups', 'updateCollectorGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectorGroupsPatchCollectorGroupByIdBodyParam = {
      name: 'Collector (Network Devices)'
    };
    describe('#patchCollectorGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchCollectorGroupById(collectorGroupsId, collectorGroupsPatchCollectorGroupByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CollectorGroups', 'patchCollectorGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectorGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCollectorGroupById(collectorGroupsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.autoBalance);
                assert.equal(true, Array.isArray(data.response.customProperties));
                assert.equal('string', data.response.userPermission);
                assert.equal(4, data.response.numOfCollectors);
                assert.equal('Collector (Network Devices)', data.response.name);
                assert.equal(4, data.response.autoBalanceInstanceCountThreshold);
                assert.equal('string', data.response.autoBalanceStrategy);
                assert.equal('Group for collectors dedicated to Network Devices', data.response.description);
                assert.equal(8, data.response.createOn);
                assert.equal(3, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CollectorGroups', 'getCollectorGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let usersId = 'fakedata';
    const usersAddAdminBodyParam = {
      roles: [
        {}
      ],
      password: samProps.authentication.password,
      email: 'john.doe@logicmonitor.com',
      username: 'John'
    };
    describe('#addAdmin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAdmin(usersAddAdminBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Doe', data.response.lastName);
                assert.equal('John Doe is an Admin on this Portal', data.response.note);
                assert.equal('{\n\n"Hosts" : true,\n"Services" : true,\n"Reports" : true,\n"Dashboards" : true,\n"Alerts" : true,\n"Settings" : true\n}', data.response.viewPermission);
                assert.equal('America/Los Angeles', data.response.timezone);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(10, data.response.lastLoginOn);
                assert.equal('string', data.response.lastActionOnLocal);
                assert.equal('sms', data.response.smsEmailFormat);
                assert.equal(false, data.response.apionly);
                assert.equal(true, Array.isArray(data.response.apiTokens));
                assert.equal('JohnDoe1', data.response.password);
                assert.equal('string', data.response.lastAction);
                assert.equal('string', data.response.trainingEmail);
                assert.equal(10, data.response.lastActionOn);
                assert.equal(1, data.response.id);
                assert.equal('john.doe@logicmonitor.com', data.response.email);
                assert.equal('email', data.response.contactMethod);
                assert.equal(10, data.response.acceptEULAOn);
                assert.equal('8054445555@logicmonitor.com', data.response.smsEmail);
                assert.equal(true, data.response.twoFAEnabled);
                assert.equal('John', data.response.firstName);
                assert.equal('8054445555', data.response.phone);
                assert.equal('Chief Admin', data.response.createdBy);
                assert.equal(true, data.response.forcePasswordChange);
                assert.equal(true, data.response.acceptEULA);
                assert.equal('John', data.response.username);
                assert.equal('active', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              usersId = data.response.id;
              saveMockData('Users', 'addAdmin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAdminList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getAdminList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateAdminByIdBodyParam = {
      roles: [
        {}
      ],
      password: samProps.authentication.password,
      email: 'john.doe@logicmonitor.com',
      username: 'John'
    };
    describe('#updateAdminById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAdminById(usersId, usersUpdateAdminByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateAdminById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPatchAdminByIdBodyParam = {
      roles: [
        {}
      ],
      password: samProps.authentication.password,
      email: 'john.doe@logicmonitor.com',
      username: 'John'
    };
    describe('#patchAdminById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAdminById(usersId, usersPatchAdminByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'patchAdminById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAdminById(usersId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Doe', data.response.lastName);
                assert.equal('John Doe is an Admin on this Portal', data.response.note);
                assert.equal('{\n\n"Hosts" : true,\n"Services" : true,\n"Reports" : true,\n"Dashboards" : true,\n"Alerts" : true,\n"Settings" : true\n}', data.response.viewPermission);
                assert.equal('America/Los Angeles', data.response.timezone);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(10, data.response.lastLoginOn);
                assert.equal('string', data.response.lastActionOnLocal);
                assert.equal('sms', data.response.smsEmailFormat);
                assert.equal(false, data.response.apionly);
                assert.equal(true, Array.isArray(data.response.apiTokens));
                assert.equal('JohnDoe1', data.response.password);
                assert.equal('string', data.response.lastAction);
                assert.equal('string', data.response.trainingEmail);
                assert.equal(3, data.response.lastActionOn);
                assert.equal(5, data.response.id);
                assert.equal('john.doe@logicmonitor.com', data.response.email);
                assert.equal('email', data.response.contactMethod);
                assert.equal(1, data.response.acceptEULAOn);
                assert.equal('8054445555@logicmonitor.com', data.response.smsEmail);
                assert.equal(false, data.response.twoFAEnabled);
                assert.equal('John', data.response.firstName);
                assert.equal('8054445555', data.response.phone);
                assert.equal('Chief Admin', data.response.createdBy);
                assert.equal(true, data.response.forcePasswordChange);
                assert.equal(true, data.response.acceptEULA);
                assert.equal('John', data.response.username);
                assert.equal('active', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getAdminById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dashboardsId = 'fakedata';
    const dashboardsAddDashboardBodyParam = {
      name: 'Default Device Group'
    };
    describe('#addDashboard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDashboard(dashboardsAddDashboardBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.owner);
                assert.equal('object', typeof data.response.template);
                assert.equal('string', data.response.userPermission);
                assert.equal(1, data.response.groupId);
                assert.equal('a new group//Collector Health/', data.response.fullName);
                assert.equal('Windows Servers Performance', data.response.description);
                assert.equal(true, data.response.sharable);
                assert.equal('object', typeof data.response.widgetsConfig);
                assert.equal('Server Dashboard', data.response.groupName);
                assert.equal('"widgetTokens":[{"name":"defaultDeviceGroup","value":"*"},{"name":"defaultServiceGroup","value":"*"}]', data.response.widgetTokens);
                assert.equal('Default Device Group', data.response.name);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.groupFullPath);
              } else {
                runCommonAsserts(data, error);
              }
              dashboardsId = data.response.id;
              saveMockData('Dashboards', 'addDashboard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDashboardList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDashboardList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'getDashboardList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsUpdateDashboardByIdBodyParam = {
      name: 'Default Device Group'
    };
    describe('#updateDashboardById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDashboardById(dashboardsId, dashboardsUpdateDashboardByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'updateDashboardById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsPatchDashboardByIdBodyParam = {
      name: 'Default Device Group'
    };
    describe('#patchDashboardById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDashboardById(dashboardsId, dashboardsPatchDashboardByIdBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'patchDashboardById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDashboardById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDashboardById(dashboardsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.owner);
                assert.equal('object', typeof data.response.template);
                assert.equal('string', data.response.userPermission);
                assert.equal(1, data.response.groupId);
                assert.equal('a new group//Collector Health/', data.response.fullName);
                assert.equal('Windows Servers Performance', data.response.description);
                assert.equal(true, data.response.sharable);
                assert.equal('object', typeof data.response.widgetsConfig);
                assert.equal('Server Dashboard', data.response.groupName);
                assert.equal('"widgetTokens":[{"name":"defaultDeviceGroup","value":"*"},{"name":"defaultServiceGroup","value":"*"}]', data.response.widgetTokens);
                assert.equal('Default Device Group', data.response.name);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.groupFullPath);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'getDashboardById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWidgetListByDashboardId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWidgetListByDashboardId(dashboardsId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'getWidgetListByDashboardId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let alertRulesId = 'fakedata';
    const alertRulesAddAlertRuleBodyParam = {
      escalatingChainId: 12,
      priority: 100,
      name: 'Warning'
    };
    describe('#addAlertRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAlertRule(alertRulesAddAlertRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('*', data.response.datapoint);
                assert.equal('*', data.response.instance);
                assert.equal('["Cisco Router"]', data.response.devices);
                assert.equal(12, data.response.escalatingChainId);
                assert.equal(100, data.response.priority);
                assert.equal(true, data.response.suppressAlertAckSdt);
                assert.equal('Port-', data.response.datasource);
                assert.equal(true, data.response.suppressAlertClear);
                assert.equal('Warning', data.response.name);
                assert.equal(2, data.response.id);
                assert.equal('Warn', data.response.levelStr);
                assert.equal('[ "Devices by Type"]', data.response.deviceGroups);
                assert.equal('object', typeof data.response.escalatingChain);
                assert.equal(15, data.response.escalationInterval);
              } else {
                runCommonAsserts(data, error);
              }
              alertRulesId = data.response.id;
              saveMockData('AlertRules', 'addAlertRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertRuleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertRuleList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal('string', data.response.searchId);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlertRules', 'getAlertRuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertRulesUpdateAlertRuleByIdBodyParam = {
      escalatingChainId: 12,
      priority: 100,
      name: 'Warning'
    };
    describe('#updateAlertRuleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertRuleById(alertRulesId, alertRulesUpdateAlertRuleByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlertRules', 'updateAlertRuleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertRulesPatchAlertRuleByIdBodyParam = {
      escalatingChainId: 12,
      priority: 100,
      name: 'Warning'
    };
    describe('#patchAlertRuleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAlertRuleById(alertRulesId, alertRulesPatchAlertRuleByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlertRules', 'patchAlertRuleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertRuleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertRuleById(alertRulesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('*', data.response.datapoint);
                assert.equal('*', data.response.instance);
                assert.equal('["Cisco Router"]', data.response.devices);
                assert.equal(12, data.response.escalatingChainId);
                assert.equal(100, data.response.priority);
                assert.equal(true, data.response.suppressAlertAckSdt);
                assert.equal('Port-', data.response.datasource);
                assert.equal(true, data.response.suppressAlertClear);
                assert.equal('Warning', data.response.name);
                assert.equal(7, data.response.id);
                assert.equal('Warn', data.response.levelStr);
                assert.equal('[ "Devices by Type"]', data.response.deviceGroups);
                assert.equal('object', typeof data.response.escalatingChain);
                assert.equal(15, data.response.escalationInterval);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlertRules', 'getAlertRuleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetricsUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetricsUsage((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.numOfStoppedAzureDevices);
                assert.equal(9, data.response.numOfServices);
                assert.equal(2, data.response.numOfTerminatedAzureDevices);
                assert.equal(5, data.response.numberOfStandardDevices);
                assert.equal(5, data.response.numOfStoppedGcpDevices);
                assert.equal(3, data.response.numOfCombinedGcpDevices);
                assert.equal(6, data.response.numOfAWSDevices);
                assert.equal(10, data.response.numberOfKubernetesDevices);
                assert.equal(1, data.response.numOfGcpDevices);
                assert.equal(1, data.response.numOfAzureDevices);
                assert.equal(6, data.response.numOfCombinedAzureDevices);
                assert.equal(3, data.response.numOfTerminatedAWSDevices);
                assert.equal(5, data.response.numOfTerminatedGcpCloudDevices);
                assert.equal(4, data.response.numOfStoppedAWSDevices);
                assert.equal(7, data.response.numberOfDevices);
                assert.equal(7, data.response.numOfConfigSourceDevices);
                assert.equal(8, data.response.numOfWebsites);
                assert.equal(5, data.response.numOfCombinedAWSDevices);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metrics', 'getMetricsUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDevicePropertyByName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDevicePropertyByName(devicesDeviceId, devicesName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteDevicePropertyByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceById(null, null, null, devicesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDatasourceById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDatasourceById(datasourcesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Datasources', 'deleteDatasourceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroupPropertyByName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceGroupPropertyByName(deviceGroupsGid, deviceGroupsName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'deleteDeviceGroupPropertyByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroupById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceGroupById(deviceGroupsId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'deleteDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceDatasourceInstanceById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceDatasourceInstanceById(datasourceInstancesDeviceId, datasourceInstancesHdsId, datasourceInstancesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DatasourceInstances', 'deleteDeviceDatasourceInstanceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpsNoteById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOpsNoteById(opsNotesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OpsNotes', 'deleteOpsNoteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebsiteById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWebsiteById(websitesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Websites', 'deleteWebsiteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEscalationChainById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEscalationChainById(escalationChainsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EscalationChains', 'deleteEscalationChainById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppliesToFunctionById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAppliesToFunctionById(appliesToFunctionsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliesToFunctions', 'deleteAppliesToFunctionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteReportById(reportId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Report', 'deleteReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCollectorById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCollectorById(collectorsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collectors', 'deleteCollectorById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroupClusterAlertConfById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceGroupClusterAlertConfById(thresholdsDeviceGroupId, thresholdsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'deleteDeviceGroupClusterAlertConfById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRecipientGroupById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRecipientGroupById(recipientGroupsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RecipientGroups', 'deleteRecipientGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportGroupById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteReportGroupById(reportGroupsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportGroups', 'deleteReportGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoleById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoleById(rolesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'deleteRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetscanById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetscanById(netscansId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netscans', 'deleteNetscanById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebsiteGroupById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWebsiteGroupById(websiteGroupsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebsiteGroups', 'deleteWebsiteGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWidgetById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWidgetById(widgetsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Widgets', 'deleteWidgetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiTokenById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiTokenById(aPITokensAdminId, aPITokensApitokenId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('APITokens', 'deleteApiTokenById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSDTById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSDTById(sDTsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDTs', 'deleteSDTById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDashboardGroupById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDashboardGroupById(dashboardGroupsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardGroups', 'deleteDashboardGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCollectorGroupById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCollectorGroupById(collectorGroupsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CollectorGroups', 'deleteCollectorGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdminById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAdminById(usersId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteAdminById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDashboardById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDashboardById(dashboardsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'deleteDashboardById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlertRuleById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAlertRuleById(alertRulesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-logicmonitor-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlertRules', 'deleteAlertRuleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
