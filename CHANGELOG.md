
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:15PM

See merge request itentialopensource/adapters/adapter-logicmonitor!14

---

## 0.4.4 [09-01-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-logicmonitor!12

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:24PM

See merge request itentialopensource/adapters/adapter-logicmonitor!11

---

## 0.4.2 [08-07-2024]

* Changes made at 2024.08.06_19:37PM

See merge request itentialopensource/adapters/adapter-logicmonitor!10

---

## 0.4.1 [07-18-2024]

* patch auth to encode base64

See merge request itentialopensource/adapters/adapter-logicmonitor!9

---

## 0.4.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!8

---

## 0.3.5 [03-27-2024]

* Changes made at 2024.03.27_13:12PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!7

---

## 0.3.4 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!6

---

## 0.3.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!5

---

## 0.3.2 [03-12-2024]

* Changes made at 2024.03.12_10:57AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!4

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:32AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!3

---

## 0.3.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!2

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!1

---

## 0.1.1 [12-09-2021]

- Initial Commit

See commit 1097d4e

---
