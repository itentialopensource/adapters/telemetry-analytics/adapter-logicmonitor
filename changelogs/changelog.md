
## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-logicmonitor!1

---

## 0.1.1 [12-09-2021]

- Initial Commit

See commit 1097d4e

---
