## Authenticating LogicMonitor Adapter 

This document will go through the steps for authenticating the LogicMonitor adapter with Custom Token Authentication (LMv1). Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

### Custom Token Authentication
The LogicMonitor adapter requires Custom Authentication (LMv1). The adapter will handle creating the LMv1 header as long as the correct API Token Access ID and API Token Access Key are provided, and `auth_method` is set to `no_authentication`. 

If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a LogicMonitor server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field
4. Note that the `username` field will contain the LMv1 `accessId` and the `password` field will contain the LMv1 `accessKey`
```json
"authentication": {
  "auth_method": "no_authentication",
  "username": "<accessId>",
  "password": "<accessKey>",
  "token_timeout": 1800000,
  "token_cache": "local",
  "invalid_token_error": 401,
  "auth_logging": false
}
```
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct accessID and accessKey.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
