## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for LogicMonitor. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for LogicMonitor.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the LogicMonitor. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceList(deviceId, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device datasource list</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDevice(start, end, netflowFilter, body, addFromWizard, callback)</td>
    <td style="padding:15px">add a new device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceList(start, end, netflowFilter, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device list</td>
    <td style="padding:15px">{base_path}/{version}/device/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertListByDeviceId(start, end, netflowFilter, id, needMessage, customColumns, bound, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get alerts</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleAutoDiscoveryByDeviceId(start, end, netflowFilter, id, callback)</td>
    <td style="padding:15px">schedule active discovery for a device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/scheduleAutoDiscovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDTHistoryByDeviceId(start, end, netflowFilter, id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get SDT history for the device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/historysdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDevice(start, end, netflowFilter, id, body, opType, callback)</td>
    <td style="padding:15px">update a device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceById(start, end, netflowFilter, id, fields, callback)</td>
    <td style="padding:15px">get device by id</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceById(start, end, netflowFilter, id, deleteHard, callback)</td>
    <td style="padding:15px">delete a device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevice(start, end, netflowFilter, id, body, opType, callback)</td>
    <td style="padding:15px">update a device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetflowFlowList(start, end, netflowFilter, id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get netflow flow list</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/flows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceAlertSettingListOfDevice(start, end, netflowFilter, deviceId, size, offset, callback)</td>
    <td style="padding:15px">get a list of alert settings for a device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/alertsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSDTListByDeviceId(start, end, netflowFilter, id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get SDTs for a device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/sdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceInstanceList(start, end, netflowFilter, id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device instance list</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnmonitoredDeviceList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get unmonitored device list</td>
    <td style="padding:15px">{base_path}/{version}/device/unmonitoreddevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceDataById(deviceId, id, period, start, end, datapoints, format, aggregate = 'first', callback)</td>
    <td style="padding:15px">get device datasource data</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDevicePropertyByName(deviceId, name, body, callback)</td>
    <td style="padding:15px">update device property</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicePropertyByName(deviceId, name, fields, callback)</td>
    <td style="padding:15px">get device property by name</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevicePropertyByName(deviceId, name, callback)</td>
    <td style="padding:15px">delete device property</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevicePropertyByName(deviceId, name, body, callback)</td>
    <td style="padding:15px">update device property</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopTalkersGraph(start, end, netflowFilter, id, format, keyword, callback)</td>
    <td style="padding:15px">get top talkers graph</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/topTalkersGraph?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetflowPortList(start, end, netflowFilter, id, ip, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get netflow port list</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetflowEndpointList(start, end, netflowFilter, id, port, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get netflow endpoint list</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceById(deviceId, id, fields, callback)</td>
    <td style="padding:15px">get device datasource</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDTHistoryByDeviceDataSourceId(deviceId, id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get SDT history for the device dataSource</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/historysdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceProperty(deviceId, body, callback)</td>
    <td style="padding:15px">add device property</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicePropertyList(deviceId, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device properties</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importConfigSource(file, callback)</td>
    <td style="padding:15px">import config source via xml</td>
    <td style="padding:15px">{base_path}/{version}/setting/configsources/importxml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorVersionList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get collector version list</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataSourceOverviewGraphById(dsId, id, callback)</td>
    <td style="padding:15px">get datasource overview graph by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/datasources/{pathv1}/ographs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataSourceOverviewGraphList(dsId, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get datasource overview graph list</td>
    <td style="padding:15px">{base_path}/{version}/setting/datasources/{pathv1}/ographs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssociatedDeviceListByDataSourceId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get devices associated with a datasource</td>
    <td style="padding:15px">{base_path}/{version}/setting/datasources/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatasourceList(format, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get datasource list</td>
    <td style="padding:15px">{base_path}/{version}/setting/datasources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatasourceById(id, format, fields, callback)</td>
    <td style="padding:15px">get datasource by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/datasources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDatasourceById(id, callback)</td>
    <td style="padding:15px">delete datasource</td>
    <td style="padding:15px">{base_path}/{version}/setting/datasources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDataSource(file, callback)</td>
    <td style="padding:15px">import datasource via xml</td>
    <td style="padding:15px">{base_path}/{version}/setting/datasources/importxml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpdateReasonListByDataSourceId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get update history for a datasource</td>
    <td style="padding:15px">{base_path}/{version}/setting/datasources/{pathv1}/updatereasons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImmediateDeviceListByDeviceGroupId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get immediate devices under group</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupDatasourceList(deviceGroupId, includeDisabledDataSourceWithoutInstance, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device group datasource list</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/datasources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAwsExternalId(callback)</td>
    <td style="padding:15px">Get AWS external id</td>
    <td style="padding:15px">{base_path}/{version}/aws/externalId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceGroupProperty(gid, body, callback)</td>
    <td style="padding:15px">add device group property</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupPropertyList(gid, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device group properties</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceGroupById(id, body, callback)</td>
    <td style="padding:15px">update device group</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupById(id, fields, callback)</td>
    <td style="padding:15px">get device group</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroupById(id, deleteChildren, deleteHard, callback)</td>
    <td style="padding:15px">delete device group</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupById(id, body, callback)</td>
    <td style="padding:15px">update device group</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceGroup(body, callback)</td>
    <td style="padding:15px">add device group</td>
    <td style="padding:15px">{base_path}/{version}/device/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device group list</td>
    <td style="padding:15px">{base_path}/{version}/device/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDTHistoryByDeviceGroupId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get SDT history for the group</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/historysdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceGroupPropertyByName(gid, name, body, callback)</td>
    <td style="padding:15px">update device group property</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupPropertyByName(gid, name, fields, callback)</td>
    <td style="padding:15px">get device group property by name</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroupPropertyByName(gid, name, callback)</td>
    <td style="padding:15px">delete device group property</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupPropertyByName(gid, name, body, callback)</td>
    <td style="padding:15px">update device group property</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceGroupDatasourceById(deviceGroupId, id, body, callback)</td>
    <td style="padding:15px">update device group datasource</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/datasources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupDatasourceById(deviceGroupId, id, fields, callback)</td>
    <td style="padding:15px">get device group datasource</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/datasources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupDatasourceById(deviceGroupId, id, body, callback)</td>
    <td style="padding:15px">update device group datasource</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/datasources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupSDTList(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device group SDTs</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/sdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertListByDeviceGroupId(id, needMessage, customColumns, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device group alerts</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceConfigSourceConfigList(deviceId, hdsId, instanceId, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get detailed config information for the instance</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceDatasourceInstanceGroup(deviceId, deviceDsId, body, callback)</td>
    <td style="padding:15px">add device datasource instance group</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceGroupList(deviceId, deviceDsId, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device datasource instance group list</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">collectDeviceConfigSourceConfig(deviceId, hdsId, instanceId, callback)</td>
    <td style="padding:15px">collect a config for a device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/config/collectNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceDatasourceInstance(deviceId, hdsId, body, callback)</td>
    <td style="padding:15px">add device instance</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceList(deviceId, hdsId, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device instance list</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceSDTHistory(deviceId, hdsId, id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get device instance SDT history</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/historysdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceDatasourceInstanceGroupById(deviceId, deviceDsId, id, body, callback)</td>
    <td style="padding:15px">update device datasource instance group</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceGroupById(deviceId, deviceDsId, id, fields, callback)</td>
    <td style="padding:15px">get device datasource instance group</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceDatasourceInstanceGroupById(deviceId, deviceDsId, id, body, callback)</td>
    <td style="padding:15px">update device datasource instance group</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceDatasourceInstanceById(deviceId, hdsId, id, body, opType, callback)</td>
    <td style="padding:15px">update device instance</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceById(deviceId, hdsId, id, fields, callback)</td>
    <td style="padding:15px">get device instance</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceDatasourceInstanceById(deviceId, hdsId, id, callback)</td>
    <td style="padding:15px">delete a device instance</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceDatasourceInstanceById(deviceId, hdsId, id, body, opType, callback)</td>
    <td style="padding:15px">update device instance</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventSourceList(format, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get eventSource list</td>
    <td style="padding:15px">{base_path}/{version}/setting/eventsources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importEventSource(file, callback)</td>
    <td style="padding:15px">import eventsource via xml</td>
    <td style="padding:15px">{base_path}/{version}/setting/eventsources/importxml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertById(id, needMessage, customColumns, fields, callback)</td>
    <td style="padding:15px">get alert</td>
    <td style="padding:15px">{base_path}/{version}/alert/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get alert list</td>
    <td style="padding:15px">{base_path}/{version}/alert/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ackAlertById(body, id, callback)</td>
    <td style="padding:15px">ack alert by id</td>
    <td style="padding:15px">{base_path}/{version}/alert/alerts/{pathv1}/ack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAlertNoteById(body, id, callback)</td>
    <td style="padding:15px">add alert note</td>
    <td style="padding:15px">{base_path}/{version}/alert/alerts/{pathv1}/note?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOpsNote(body, callback)</td>
    <td style="padding:15px">add opsnote</td>
    <td style="padding:15px">{base_path}/{version}/setting/opsnotes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsNoteList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get opsnote list</td>
    <td style="padding:15px">{base_path}/{version}/setting/opsnotes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOpsNoteById(id, body, callback)</td>
    <td style="padding:15px">update opsnote</td>
    <td style="padding:15px">{base_path}/{version}/setting/opsnotes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsNoteById(id, fields, callback)</td>
    <td style="padding:15px">get opsnote by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/opsnotes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpsNoteById(id, callback)</td>
    <td style="padding:15px">delete opsnote</td>
    <td style="padding:15px">{base_path}/{version}/setting/opsnotes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOpsNoteById(id, body, callback)</td>
    <td style="padding:15px">update opsnote</td>
    <td style="padding:15px">{base_path}/{version}/setting/opsnotes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDTHistoryByWebsiteId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get SDT history for the website</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}/historysdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchWebsiteById(id, body, opType, callback)</td>
    <td style="padding:15px">update website</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteById(id, format, callback)</td>
    <td style="padding:15px">get website by id</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebsiteById(id, callback)</td>
    <td style="padding:15px">delete website</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWebsiteById(id, body, opType, callback)</td>
    <td style="padding:15px">update website</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsitePropertyListByWebsiteId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get a list of properties for a website</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteAlertListByWebsiteId(id, needMessage, customColumns, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get alerts for a website</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWebsite(body, callback)</td>
    <td style="padding:15px">add website</td>
    <td style="padding:15px">{base_path}/{version}/website/websites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteList(collectorIds, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get website list</td>
    <td style="padding:15px">{base_path}/{version}/website/websites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteSDTListByWebsiteId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get a list of SDTs for a website</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}/sdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteDataByGraphName(id, start, end, graphName, format, callback)</td>
    <td style="padding:15px">get website data by graph name</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}/graphs/{pathv2}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteMonitorCheckPointList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get website checkpoint list</td>
    <td style="padding:15px">{base_path}/{version}/website/smcheckpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceConfigSourceConfigById(deviceId, hdsId, instanceId, id, format, startEpoch, fields, callback)</td>
    <td style="padding:15px">get a config for a device</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/config/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceGraphData(deviceId, hdsId, id, start, end, graphId, format, callback)</td>
    <td style="padding:15px">get device instance graph data</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/graphs/{pathv4}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceGroupOverviewGraphData(deviceId, deviceDsId, dsigId, start, end, ographId, format, callback)</td>
    <td style="padding:15px">get device instance group overview graph data</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/groups/{pathv3}/graphs/{pathv4}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteGraphData(websiteId, checkpointId, start, end, graphName, format, callback)</td>
    <td style="padding:15px">get website graph data</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}/checkpoints/{pathv2}/graphs/{pathv3}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceInstanceGraphDataOnlyByInstanceId(instanceId, graphId, start, end, format, callback)</td>
    <td style="padding:15px">get device instance data</td>
    <td style="padding:15px">{base_path}/{version}/device/devicedatasourceinstances/{pathv1}/graphs/{pathv2}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceData(deviceId, hdsId, id, period, start, end, datapoints, format, callback)</td>
    <td style="padding:15px">get device instance data</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWidgetDataById(start, end, format, id, callback)</td>
    <td style="padding:15px">get widget data</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/widgets/{pathv1}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteCheckpointDataById(srvId, checkId, period, start, end, datapoints, format, aggregate = 'first', callback)</td>
    <td style="padding:15px">get data for a website checkpoint</td>
    <td style="padding:15px">{base_path}/{version}/website/websites/{pathv1}/checkpoints/{pathv2}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchDeviceInstancesData(body, period, start, end, aggregate = 'first', callback)</td>
    <td style="padding:15px">fetch device instances data</td>
    <td style="padding:15px">{base_path}/{version}/device/instances/datafetch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogById(id, callback)</td>
    <td style="padding:15px">Get audit log by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/accesslogs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogList(format, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">Get audit logs</td>
    <td style="padding:15px">{base_path}/{version}/setting/accesslogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEscalationChain(body, callback)</td>
    <td style="padding:15px">add escalation chain</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/chains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEscalationChainList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get escalation chain list</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/chains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchEscalationChainById(id, body, callback)</td>
    <td style="padding:15px">update escalation chain</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/chains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEscalationChainById(id, fields, callback)</td>
    <td style="padding:15px">get escalation chain by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/chains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEscalationChainById(id, callback)</td>
    <td style="padding:15px">delete escalation chain</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/chains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEscalationChainById(id, body, callback)</td>
    <td style="padding:15px">update escalation chain</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/chains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppliesToFunction(body, id, reason, ignoreReference, callback)</td>
    <td style="padding:15px">update applies to function</td>
    <td style="padding:15px">{base_path}/{version}/setting/functions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliesToFunctionById(id, callback)</td>
    <td style="padding:15px">get applies to function</td>
    <td style="padding:15px">{base_path}/{version}/setting/functions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppliesToFunctionById(id, ignoreReference, callback)</td>
    <td style="padding:15px">delete applies to function</td>
    <td style="padding:15px">{base_path}/{version}/setting/functions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAppliesToFunction(body, id, reason, ignoreReference, callback)</td>
    <td style="padding:15px">update applies to function</td>
    <td style="padding:15px">{base_path}/{version}/setting/functions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAppliesToFunction(body, callback)</td>
    <td style="padding:15px">add applies to function</td>
    <td style="padding:15px">{base_path}/{version}/setting/functions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliesToFunctionList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get applies to function list</td>
    <td style="padding:15px">{base_path}/{version}/setting/functions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addReport(body, callback)</td>
    <td style="padding:15px">add report</td>
    <td style="padding:15px">{base_path}/{version}/report/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get report list</td>
    <td style="padding:15px">{base_path}/{version}/report/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReportById(id, body, callback)</td>
    <td style="padding:15px">update report</td>
    <td style="padding:15px">{base_path}/{version}/report/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportById(id, fields, callback)</td>
    <td style="padding:15px">get report by id</td>
    <td style="padding:15px">{base_path}/{version}/report/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportById(id, callback)</td>
    <td style="padding:15px">delete report</td>
    <td style="padding:15px">{base_path}/{version}/report/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportById(id, body, callback)</td>
    <td style="padding:15px">update report</td>
    <td style="padding:15px">{base_path}/{version}/report/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateReportById(body, id, callback)</td>
    <td style="padding:15px">run a report</td>
    <td style="padding:15px">{base_path}/{version}/report/reports/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCollector(body, callback)</td>
    <td style="padding:15px">add collector</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get collector list</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCollectorById(id, body, forceUpdateFailedOverDevices, callback)</td>
    <td style="padding:15px">update collector</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorById(id, fields, callback)</td>
    <td style="padding:15px">get collector</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCollectorById(id, callback)</td>
    <td style="padding:15px">delete collector</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCollectorById(id, body, forceUpdateFailedOverDevices, callback)</td>
    <td style="padding:15px">update collector</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorInstaller(collectorId, osAndArch, collectorVersion, token, monitorOthers, collectorSize, useEA, callback)</td>
    <td style="padding:15px">get collector installer</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors/{pathv1}/installers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ackCollectorDownAlertById(id, body, callback)</td>
    <td style="padding:15px">ack collector down alert</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/collectors/{pathv1}/ackdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceAlertSettingListOfDSI(deviceId, hdsId, instanceId, size, offset, callback)</td>
    <td style="padding:15px">get a list of alert settings for a device datasource instance</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/alertsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceGroupClusterAlertConf(deviceGroupId, body, callback)</td>
    <td style="padding:15px">Add cluster alert configuration</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/clusterAlertConf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupClusterAlertConfList(deviceGroupId, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get a list of cluster alert configurations for a device group</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/clusterAlertConf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceGroupDatasourceAlertSetting(deviceGroupId, dsId, body, callback)</td>
    <td style="padding:15px">update device group datasource alert setting</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/datasources/{pathv2}/alertsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupDatasourceAlertSetting(deviceGroupId, dsId, fields, callback)</td>
    <td style="padding:15px">get device group datasource alert setting</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/datasources/{pathv2}/alertsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupDatasourceAlertSetting(deviceGroupId, dsId, body, callback)</td>
    <td style="padding:15px">update device group datasource alert setting</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/datasources/{pathv2}/alertsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceDatasourceInstanceAlertSettingById(deviceId, hdsId, instanceId, id, body, callback)</td>
    <td style="padding:15px">update device instance alert setting</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/alertsettings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDatasourceInstanceAlertSettingById(deviceId, hdsId, instanceId, id, fields, callback)</td>
    <td style="padding:15px">get device instance alert setting</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/alertsettings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceDatasourceInstanceAlertSettingById(deviceId, hdsId, instanceId, id, body, callback)</td>
    <td style="padding:15px">update device instance alert setting</td>
    <td style="padding:15px">{base_path}/{version}/device/devices/{pathv1}/devicedatasources/{pathv2}/instances/{pathv3}/alertsettings/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceGroupClusterAlertConfById(deviceGroupId, id, body, callback)</td>
    <td style="padding:15px">Update cluster alert configuration</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/clusterAlertConf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupClusterAlertConfById(deviceGroupId, id, callback)</td>
    <td style="padding:15px">Get cluster alert configuration by id</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/clusterAlertConf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroupClusterAlertConfById(deviceGroupId, id, callback)</td>
    <td style="padding:15px">Delete cluster alert configuration</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/clusterAlertConf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupClusterAlertConfById(deviceGroupId, id, body, callback)</td>
    <td style="padding:15px">Update cluster alert configuration</td>
    <td style="padding:15px">{base_path}/{version}/device/groups/{pathv1}/clusterAlertConf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRecipientGroupById(id, body, callback)</td>
    <td style="padding:15px">update recipient group</td>
    <td style="padding:15px">{base_path}/{version}/setting/recipientgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecipientGroupById(id, callback)</td>
    <td style="padding:15px">get recipient group by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/recipientgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRecipientGroupById(id, callback)</td>
    <td style="padding:15px">delete recipient group</td>
    <td style="padding:15px">{base_path}/{version}/setting/recipientgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRecipientGroupById(id, body, callback)</td>
    <td style="padding:15px">update recipient group</td>
    <td style="padding:15px">{base_path}/{version}/setting/recipientgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRecipientGroup(body, callback)</td>
    <td style="padding:15px">add recipient group</td>
    <td style="padding:15px">{base_path}/{version}/setting/recipientgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecipientGroupList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get recipient group List</td>
    <td style="padding:15px">{base_path}/{version}/setting/recipientgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addReportGroup(body, callback)</td>
    <td style="padding:15px">add report group</td>
    <td style="padding:15px">{base_path}/{version}/report/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportGroupList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get report group list</td>
    <td style="padding:15px">{base_path}/{version}/report/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchReportGroupById(id, body, callback)</td>
    <td style="padding:15px">update report group</td>
    <td style="padding:15px">{base_path}/{version}/report/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportGroupById(id, callback)</td>
    <td style="padding:15px">get report group by id</td>
    <td style="padding:15px">{base_path}/{version}/report/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportGroupById(id, callback)</td>
    <td style="padding:15px">delete report group</td>
    <td style="padding:15px">{base_path}/{version}/report/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportGroupById(id, body, callback)</td>
    <td style="padding:15px">update report group</td>
    <td style="padding:15px">{base_path}/{version}/report/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRoleById(id, body, callback)</td>
    <td style="padding:15px">update role</td>
    <td style="padding:15px">{base_path}/{version}/setting/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleById(id, fields, callback)</td>
    <td style="padding:15px">get role by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoleById(id, callback)</td>
    <td style="padding:15px">delete role</td>
    <td style="padding:15px">{base_path}/{version}/setting/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoleById(id, body, callback)</td>
    <td style="padding:15px">update role</td>
    <td style="padding:15px">{base_path}/{version}/setting/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRole(body, callback)</td>
    <td style="padding:15px">add role</td>
    <td style="padding:15px">{base_path}/{version}/setting/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get role list</td>
    <td style="padding:15px">{base_path}/{version}/setting/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetscan(body, callback)</td>
    <td style="padding:15px">add a new netscan</td>
    <td style="padding:15px">{base_path}/{version}/setting/netscans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetscanList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get netscan list</td>
    <td style="padding:15px">{base_path}/{version}/setting/netscans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetscan(body, reason, id, callback)</td>
    <td style="padding:15px">update a netscan</td>
    <td style="padding:15px">{base_path}/{version}/setting/netscans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetscanById(id, callback)</td>
    <td style="padding:15px">get netscan by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/netscans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetscanById(id, callback)</td>
    <td style="padding:15px">delete a netscan</td>
    <td style="padding:15px">{base_path}/{version}/setting/netscans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetscan(body, reason, id, callback)</td>
    <td style="padding:15px">update a netscan</td>
    <td style="padding:15px">{base_path}/{version}/setting/netscans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchWebsiteGroupById(id, body, opType, callback)</td>
    <td style="padding:15px">update website group</td>
    <td style="padding:15px">{base_path}/{version}/website/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteGroupById(id, callback)</td>
    <td style="padding:15px">get website group</td>
    <td style="padding:15px">{base_path}/{version}/website/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebsiteGroupById(id, deleteChildren, callback)</td>
    <td style="padding:15px">delete website group</td>
    <td style="padding:15px">{base_path}/{version}/website/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWebsiteGroupById(id, body, opType, callback)</td>
    <td style="padding:15px">update website group</td>
    <td style="padding:15px">{base_path}/{version}/website/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSDTListByWebsiteGroupId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get a list of SDTs for a website group</td>
    <td style="padding:15px">{base_path}/{version}/website/groups/{pathv1}/sdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImmediateWebsiteListByWebsiteGroupId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get a list of websites for a group</td>
    <td style="padding:15px">{base_path}/{version}/website/groups/{pathv1}/websites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDTHistoryByWebsiteGroupId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get SDT history for the website group</td>
    <td style="padding:15px">{base_path}/{version}/website/groups/{pathv1}/historysdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWebsiteGroup(body, callback)</td>
    <td style="padding:15px">add website group</td>
    <td style="padding:15px">{base_path}/{version}/website/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebsiteGroupList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get website group list</td>
    <td style="padding:15px">{base_path}/{version}/website/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeDebugCommand(body, collectorId, callback)</td>
    <td style="padding:15px">Execute a Collector debug command</td>
    <td style="padding:15px">{base_path}/{version}/debug?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDebugCommandResult(id, collectorId, callback)</td>
    <td style="padding:15px">Get the result of a Collector debug command</td>
    <td style="padding:15px">{base_path}/{version}/debug/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importBatchJob(file, callback)</td>
    <td style="padding:15px">import batch job via xml</td>
    <td style="padding:15px">{base_path}/{version}/setting/batchjobs/importxml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchWidgetById(id, body, callback)</td>
    <td style="padding:15px">update widget</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/widgets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWidgetById(id, fields, callback)</td>
    <td style="padding:15px">get widget by id</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/widgets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWidgetById(id, callback)</td>
    <td style="padding:15px">delete widget</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/widgets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWidgetById(id, body, callback)</td>
    <td style="padding:15px">update widget</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/widgets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWidget(body, callback)</td>
    <td style="padding:15px">add widget</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/widgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWidgetList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get widget list</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/widgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addApiTokenByAdminId(adminId, body, type, callback)</td>
    <td style="padding:15px">add api tokens for a user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}/apitokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiTokenListByAdminId(adminId, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get api tokens for a user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}/apitokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiTokenByAdminId(adminId, apitokenId, body, callback)</td>
    <td style="padding:15px">update api tokens for a user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}/apitokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiTokenById(adminId, apitokenId, callback)</td>
    <td style="padding:15px">delete apiToken</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}/apitokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApiTokenByAdminId(adminId, apitokenId, body, callback)</td>
    <td style="padding:15px">update api tokens for a user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}/apitokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiTokenList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get a list of api tokens across users</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/apitokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalApiStats(callback)</td>
    <td style="padding:15px">get external api stats info</td>
    <td style="padding:15px">{base_path}/{version}/apiStats/externalApis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSDT(body, callback)</td>
    <td style="padding:15px">add SDT</td>
    <td style="padding:15px">{base_path}/{version}/sdt/sdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDTList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get SDT list</td>
    <td style="padding:15px">{base_path}/{version}/sdt/sdts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSDTById(id, body, callback)</td>
    <td style="padding:15px">update SDT</td>
    <td style="padding:15px">{base_path}/{version}/sdt/sdts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDTById(id, fields, callback)</td>
    <td style="padding:15px">get SDT by id</td>
    <td style="padding:15px">{base_path}/{version}/sdt/sdts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSDTById(id, callback)</td>
    <td style="padding:15px">delete SDT</td>
    <td style="padding:15px">{base_path}/{version}/sdt/sdts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSDTById(id, body, callback)</td>
    <td style="padding:15px">update SDT</td>
    <td style="padding:15px">{base_path}/{version}/sdt/sdts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDNSMapping(file, callback)</td>
    <td style="padding:15px">import DNS mapping via CSV</td>
    <td style="padding:15px">{base_path}/{version}/setting/dnsmappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDashboardGroupById(id, body, callback)</td>
    <td style="padding:15px">update dashboard group</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardGroupById(id, template, format, fields, callback)</td>
    <td style="padding:15px">get dashboard group</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDashboardGroupById(id, allowNonEmptyGroup, callback)</td>
    <td style="padding:15px">delete dashboard group</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDashboardGroupById(id, body, callback)</td>
    <td style="padding:15px">update dashboard group</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDashboardGroup(body, callback)</td>
    <td style="padding:15px">add dashboard group</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardGroupList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get dashboard group list</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCollectorGroup(body, callback)</td>
    <td style="padding:15px">add collector group</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorGroupList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get collector group list</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCollectorGroupById(id, body, forceUpdateFailedOverDevices, callback)</td>
    <td style="padding:15px">update collector group</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorGroupById(id, fields, callback)</td>
    <td style="padding:15px">get collector group</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCollectorGroupById(id, callback)</td>
    <td style="padding:15px">delete collector group</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCollectorGroupById(id, body, forceUpdateFailedOverDevices, callback)</td>
    <td style="padding:15px">update collector group</td>
    <td style="padding:15px">{base_path}/{version}/setting/collector/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAdminById(id, body, changePassword, callback)</td>
    <td style="padding:15px">update user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminById(id, fields, callback)</td>
    <td style="padding:15px">get user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdminById(id, callback)</td>
    <td style="padding:15px">delete user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAdminById(id, body, changePassword, callback)</td>
    <td style="padding:15px">update user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAdmin(body, callback)</td>
    <td style="padding:15px">add user</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get user list</td>
    <td style="padding:15px">{base_path}/{version}/setting/admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDashboard(body, callback)</td>
    <td style="padding:15px">add dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get dashboard list</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWidgetListByDashboardId(id, fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get widget list by DashboardId</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/dashboards/{pathv1}/widgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDashboardById(id, body, overwriteGroupFields, callback)</td>
    <td style="padding:15px">update dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardById(id, template, format, fields, callback)</td>
    <td style="padding:15px">get dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDashboardById(id, callback)</td>
    <td style="padding:15px">delete dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDashboardById(id, body, overwriteGroupFields, callback)</td>
    <td style="padding:15px">update dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAlertRuleById(id, body, callback)</td>
    <td style="padding:15px">update alert rule</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertRuleById(id, fields, callback)</td>
    <td style="padding:15px">get alert rule by id</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlertRuleById(id, callback)</td>
    <td style="padding:15px">delete alert rule</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertRuleById(id, body, callback)</td>
    <td style="padding:15px">update alert rule</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAlertRule(body, callback)</td>
    <td style="padding:15px">add alert rule</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertRuleList(fields, size, offset, filter, callback)</td>
    <td style="padding:15px">get alert rule list</td>
    <td style="padding:15px">{base_path}/{version}/setting/alert/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetricsUsage(callback)</td>
    <td style="padding:15px">get metrics usage</td>
    <td style="padding:15px">{base_path}/{version}/metrics/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
