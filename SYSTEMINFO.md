# LogicMonitor

Vendor: LogicMonitor
Homepage: https://www.logicmonitor.com/

Product: LogicMonitor
Product Page: https://www.logicmonitor.com/

## Introduction
We classify LogicMonitor into the Service Assurance domain as LogicMonitor provides visibility into the performance and health of networks, servers, applications, and cloud services. 

## Why Integrate
The LogicMonitor adapter from Itential is used to integrate the Itential Automation Platform (IAP) with LogicMonitor. With this adapter you have the ability to perform operations on items such as:

- Devices
- Thresholds
- Metrics

## Additional Product Documentation
[API Documents for LogicMonitor](https://www.logicmonitor.com/support/rest-api-developers-guide/overview/using-logicmonitors-rest-api)